#!/bin/sh

###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:		/app/template/src/init.sh
#
# Description:	Application Template - Initialisation Script
#
# Author:	Florian Kemser
#		florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#		Faculty of Electrical and Computer Engineering
#		Institute of Communication Technology
#		Deutsche Telekom Chair of Communication Networks
#
# Date:		Aug 2019 - Mar 2020
###########################################################################################

###########################################################################################
######################################## CONSTANTS ########################################
###########################################################################################
DIR_ROOT="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
###########################################################################################

###########################################################################################
######################################## VARIABLES ########################################
###########################################################################################
command="" 	# command to start test application
###########################################################################################

###########################################################################################
################################## START MODIFYING HERE ###################################
###########################################################################################
# You can add further modes by adding more cases to the statement.
# In case you use file or foldernames they must be located in </app/your_application/src>.
# Please make sure that the variables used in this file match the environmental variables
# defined in your Dockerfile </app/your_application/docker/Dockerfile> .
case $MODE in
	receiver)
		command="(command) receiver.file $ENV1 $ENV2" # Set the receiver command here
		;;
	sender)
		command="(command) sender.file $ENV1 $ENV2" # Set the sender command here.
		;;
esac
###########################################################################################
################################### STOP MODIFYING HERE ###################################
###########################################################################################

###########################################################################################
############################ PLEASE DO NOT MODIFY THIS SECTION ############################
###########################################################################################
"$DIR_ROOT/wrapper.sh" \
	--command "$command" \
	--stress-cpu "${STRESS_CPU}" \
	--stress-cpu-method "${STRESS_CPU_METHOD}" \
	--stress-ram "${STRESS_RAM}"
###########################################################################################
