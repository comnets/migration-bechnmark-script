#!/bin/sh

###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:		udpmessenger/src/init.sh
#
# Description:	Video Recognition Application initialisation script
#
# Author:	Florian Kemser
#		florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#		Faculty of Electrical and Computer Engineering
#		Institute of Communication Technology
#		Deutsche Telekom Chair of Communication Networks
#
# Date:		Feb 2020
###########################################################################################

###########################################################################################
######################################## CONSTANTS ########################################
###########################################################################################
DIR_ROOT="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
###########################################################################################

###########################################################################################
######################################## VARIABLES ########################################
###########################################################################################
command=""
###########################################################################################

# >>>> Start individual changes
case $MODE in
	receiver)
		# Receiver mode
		if ! [ -z ${PING_IP} ]
		then
			# UDP ping application (for determining the downtime)
			python3 -u pingudp.py "${PING_IP}" "${PING_PORT}" "${PING_INTERVAL}" &
		fi
		command="python3 -u ${MODE}.py ${IP} ${PORT} ${PKG_DELAY_MAX}"
		;;
	sender)
		command="python3 -u ${MODE}.py ${IP} ${PORT} ${VID_WIDTH} ${VID_HEIGHT} ${VID_FPS}"
		;;
esac
# <<<< End individual changes

"$DIR_ROOT/wrapper.sh" --command "$command" --stress-cpu "${STRESS_CPU}" --stress-cpu-method "${STRESS_CPU_METHOD}" --stress-ram "${STRESS_RAM}"
