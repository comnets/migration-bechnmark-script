###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:			vidrecognition/src/receiver.py
#
# Description:	Video Recognition Application (receiver)
#
# Authors:		David Kuß
#				david.kuss@mailbox.tu-dresden.de
#
#				Florian Kemser
#				florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#				Faculty of Electrical and Computer Engineering
#				Institute of Communication Technology
#				Deutsche Telekom Chair of Communication Networks
#
# Date:			Aug 2019 - Jan 2020
###########################################################################################


import time
import socket
from collections import deque
import cv2 as cv
import numpy as np
import threading
import sys

sockaddr = (str(sys.argv[1]), int(sys.argv[2]))
package_delay_max = (float(sys.argv[3]))
imgToShow=None
lock = threading.Lock()

def UDPPictureReceiver():
	global imgToShow
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	sock.bind(sockaddr)
	print("...binding done [" + str(sockaddr) + "] ...")
	prev_time = time.time()
	delay_q = deque(maxlen=100)
	data_q = deque(maxlen=100)
	imgTrans = []
	rx_len = 0
	package_time_prev = time.time()

	while True:
		buf = sock.recvfrom(1472)
		package_time_curr = time.time()
		package_delay = (package_time_curr - package_time_prev)
		if (package_delay > package_delay_max ):
			print("Delay: {:02.03f} ms between {} and {}".format(package_delay*1000,package_time_prev,package_time_curr))
		if buf[0] == b"[##picture##start]":
			rx_len = 0
			imgTrans = []
		elif buf[0][:16] == b"[##picture##end]":
			rx_len_expected = int.from_bytes(buf[0][16:20], byteorder="big")
			framewidth = int.from_bytes(buf[0][20:24], byteorder="big")
			frameheight = int.from_bytes(buf[0][24:28], byteorder="big")
			#print(str(rx_len_expected),str(framewidth),str(frameheight))
			if rx_len_expected != rx_len:
				print(str(rx_len)+"\tinvalid length; expected "+str(rx_len_expected))
			else:
				time_taken = (time.time() - prev_time)
				delay_q.append(time_taken)
				data_q.append(rx_len)
				average_delay = sum(delay_q) / len(delay_q)
				average_throughput = (sum(data_q) * 8) / (sum(delay_q) * 1000000)  # in Mbps
				print("{}\t{:02.03f}ms\t\t{:02.03f}ms\t\t{:02.04f}Mbps".format(rx_len, time_taken * 1000, average_delay * 1000, average_throughput))
				with lock:
					imgToShow = np.frombuffer(np.array(imgTrans).ravel(), dtype=np.uint8, count=rx_len).reshape((frameheight,framewidth,3))
				imgTrans = []
				#time.sleep(0.5)
				#cv.imwrite("./transmitted.png", imgToShow)

			prev_time = time.time()
		else:
			rx_len = rx_len + len(buf[0])
			imgTrans.append(buf[0])

		package_time_prev = package_time_curr

print("UDP PICTURE RECEIVER :: started ...")
listenUDPReceiver = threading.Thread(target=UDPPictureReceiver)
listenUDPReceiver.start()

# 2019-10-28: Added
hostname = socket.gethostname()
ipaddr = str(sys.argv[1])

while True:
	if imgToShow is not None:
		cv.imshow("Host: " + hostname + " (" + ipaddr + ") - Transmitted", imgToShow.copy())
	key = cv.waitKey(1) & 0xFF
	if key == ord("q"):
		break
	time.sleep(0.03)


cv.destroyAllWindows()
