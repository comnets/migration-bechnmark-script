#!/bin/sh

###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:		src/helpers.sh
#
# Description:	Helper Script
#
# Author:	Florian Kemser
#		florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#		Faculty of Electrical and Computer Engineering
#		Institute of Communication Technology
#		Deutsche Telekom Chair of Communication Networks
#
# Date:		Feb 2020
###########################################################################################

###########################################################################################
######################################## ATTENTION ########################################
###########################################################################################
# To adapt the script to your testbed please use the configuration file run.sh.options
# and do not make any changes in this file unless you would like to bugfix or modify
# this script.
###########################################################################################

###########################################################################################
######################################## FUNCTIONS ########################################
###########################################################################################

###########################################################################################
# NAME
#	h_checkRequirements - checks if all hardware/software requirements are met
#
# SYNOPSIS
#	h_checkRequirements
#
# DESCRIPTION
#	Checks if the hardware/software setup fulfills the requirements
#	and sets the range for certain arguments.
#
# ENVIRONMENT
#	ARG_A_MIN, ARG_A_MAX
#		sets the ARG_A value range
h_checkRequirements() {
	for const in ARG_A_MIN ARG_A_MAX
	do
		eval $const=""
	done
}
###########################################################################################

###########################################################################################
# NAME
#	h_convertUnit - converts a given value from one unit into another one
#
# SYNOPSIS
#	h_convertUnit [-i] -u UNIT value
#
# DESCRIPTION
#	If value has no unit, then Bytes (b) is assumed
#
# OPTIONS
#	-i, --include-unit
#		include unit in the return value
#
#	-u, --unit UNIT
#		convert value into UNIT
#
# EXIT STATUS
#	0	success
#
#	1	error: value is not a number
#
#	2	error: value has unknown unit
#
#	3	error: unknown target unit
#
#
# RETURN VALUE
#	""	(if exit code != 0)
#
#	converted value
#		(if exit status == 0)
h_convertUnit() {
	# OPTIONS
	local ARG_INCLUDE=""
	local ARG_UNIT=""
	local ARG_VALUE=""

	# EXIT STATUS
	local EXIT_SUCCESS="0"
	local EXIT_NAN="1"
	local EXIT_VALUE_UNIT="2"
	local EXIT_TARGET_UNIT="3"

	# Constants
	local UNIT_DEFAULT="b" # default unit if no unit is given

	# Variables
	local num=""	# value (number)
	local unit=""	# value (unit)
	local fact=""	# unit factor
	local div=""	# unit divisor

	# Read arguments
	while [ $# -gt 1 ]
	do
		case $1 in
			-i|--include-unit)
				ARG_INCLUDE="1"
				;;
			-u|--unit)
				ARG_UNIT="$2"
				shift
				;;
			*)
				return $EXIT_NAN
				;;
		esac
		shift
	done

	ARG_VALUE="$1"

	if [ $ARG_VALUE -le 0 -o $ARG_VALUE -gt 0 ] 2>/dev/null
	then
		num="$ARG_VALUE"
		unit="$UNIT_DEFAULT"
	else
		num="$(echo $ARG_VALUE | rev | cut -c 2- | rev)"
		unit="$(echo $ARG_VALUE | rev | cut -c 1)"

		if ! [ $num -le 0 -o $num -gt 0 ] 2>/dev/null
		then
			echo ""
			return $EXIT_NAN
		fi
	fi

	case $unit in
		b )
			fact=1
			;;
		k )
			fact=1000
			;;
		m )
			fact=1000000
			;;
		g )
			fact=1000000000
			;;
		K )
			fact=8000
			;;
		M )
			fact=8000000
			;;
		*)
			echo ""
			return $EXIT_VALUE_UNIT
			;;
	esac

	case $ARG_UNIT in
		b )
			div=1
			;;
		k )
			div=1000
			;;
		m )
			div=1000000
			;;
		g )
			div=1000000000
			;;
		* )
			echo ""
			return $EXIT_TARGET_UNIT
			;;
	esac

	if [ -z $ARG_INCLUDE ]
	then
		echo "$(expr $num \* $fact / $div)"
	else
		echo "$(expr $num \* $fact / $div)${ARG_UNIT}"
	fi

	return $EXIT_SUCCESS

}
###########################################################################################

###########################################################################################
# NAME
#	h_isBetweenRange - checks if a given value is between a defined range
#
# SYNOPSIS
#	h_isBetweenRange [-mM] value
#
# DESCRIPTION
#	Checks if value is between a defined range.
#	At least a minimum or a maximum has to be defined, see OPTIONS.
#
# OPTIONS
#	-m, --min MIN
#		minimum allowed value (MAX with or without unit)
#
#	-M, --max MAX
#		maximum allowed value (MAX with or without unit)
#
# EXIT STATUS
#	0	success, value within range
#
#	1	general error
#
#	2	error, value or MIN or MAX has unknown unit
#
#	3	error, value below minimum
#
#	4	error, value above maximum
h_isBetweenRange() {
	# OPTIONS
	local ARG_MIN=""
	local ARG_MAX=""
	local ARG_VALUE=""

	# EXIT STATUS
	local EXIT_SUCCESS="0"
	local EXIT_ERR="1"
	local EXIT_UNKNOWN_UNIT="2"
	local EXIT_BELOW_MIN="3"
	local EXIT_ABOVE_MAX="4"

	# Variables
	local fact=""
	local num=""
	local unit=""

	# Read arguments
	while [ $# -gt 1 ]
	do
		case $1 in
			-m|--min)
				ARG_MIN="$2"
				shift
				;;
			-M|--max)
				ARG_MAX="$2"
				shift
				;;
			*)
				return $EXIT_ERR
				;;
		esac
		shift
	done

	ARG_VALUE="$1"

	if ! [ $ARG_VALUE -le 0 -o $ARG_VALUE -gt 0 ] 2>/dev/null
	then
		unit="$(echo $ARG_VALUE | rev | cut -c 1)"
		num="$(h_convertUnit --unit $unit "$ARG_VALUE")"

		if [ $? -ne 0 ]
		then
			return $EXIT_UNKNOWN_UNIT
		fi
	else
		num="$ARG_VALUE"
	fi

	if [ ! -z $ARG_MIN ]
	then
		if [ ! -z $unit ]
		then
			ARG_MIN="$(h_convertUnit --unit $unit "$ARG_MIN")"
			if [ $? -ne 0 ]
			then
	  			return $EXIT_UNKNOWN_UNIT
			fi
		fi

		if [ $num -lt $ARG_MIN ] 2>/dev/null
		then
			return $EXIT_BELOW_MIN
		fi
	fi

	if [ ! -z $ARG_MAX ]
	then
		if [ ! -z $unit ]
		then
			ARG_MAX="$(h_convertUnit --unit $unit "$ARG_MAX")"
			if [ $? -ne 0 ]
			then
				return $EXIT_UNKNOWN_UNIT
			fi
		fi

		if [ $num -gt $ARG_MAX ] 2>/dev/null
		then
			return $EXIT_ABOVE_MAX
		fi
	fi

	return 0
}
###########################################################################################

###########################################################################################
# NAME
#	h_commandExists - checks if the given command exists
#
# SYNOPSIS
#	h_commandExists command
#
# DESCRIPTION
#	command without any arguments
#
# EXIT STATUS
#	0	success: command found
#
#	1	error: command not found
h_commandExists() {
	command -v $@ >/dev/null
	return $?
}
###########################################################################################
