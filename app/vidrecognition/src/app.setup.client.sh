#!/bin/bash

###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:		vidrecognition/src/setup_client.sh
#
# Description:	Video Recognition Application Cam Reset Script
#
# Author:	Florian Kemser
#		florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#		Faculty of Electrical and Computer Engineering
#		Institute of Communication Technology
#		Deutsche Telekom Chair of Communication Networks
#
# Date:		Feb 2020
###########################################################################################

###########################################################################################
########################################## MAIN ###########################################
###########################################################################################
# Resets the connected cam
#
# Requires: $BIN_USBRESET $CLIENT_WEBCAM_DEVID (defined in vidrecognition/app.sh.options)

local client_webcam_path="/dev/bus/usb/$(lsusb | grep $APP_WEBCAM_DEVID | cut -d ':' -f1 | cut -d ' ' -f2,4 | sed 's/ /\//g')"
sudo "$APP_BIN_USBRESET" "$client_webcam_path" >/dev/null
