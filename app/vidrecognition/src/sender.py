###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:			vidrecognition/src/sender.py
#
# Description:	Video Recognition Application (sender)
#
# Authors:		David Kuß
#				david.kuss@mailbox.tu-dresden.de
#
#				Florian Kemser
#				florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#				Faculty of Electrical and Computer Engineering
#				Institute of Communication Technology
#				Deutsche Telekom Chair of Communication Networks
#
# Date:			Aug 2019 - Jan 2020
###########################################################################################

import time
import socket
from collections import deque
import cv2 as cv
import numpy as np
import sys


print("UDP PICTURE RECEIVER :: started ...")
sockaddr = (str(sys.argv[1]), int(sys.argv[2]))

# initialize the video stream and allow the cammera sensor to warmup
vcap = None
print("[INFO] starting video stream...")
try:
	vcap = cv.VideoCapture(0)
	vcap.set(cv.CAP_PROP_FRAME_WIDTH, int(sys.argv[3]))
	vcap.set(cv.CAP_PROP_FRAME_HEIGHT, int(sys.argv[4]))
	vcap.set(cv.CAP_PROP_FPS, int(sys.argv[5]))
	vcap.set(cv.CAP_PROP_FOURCC, cv.VideoWriter_fourcc('M', 'J', 'P', 'G'))
finally:
	if vcap.isOpened() == False:
		print("[ERROR] video stream not found - web cam issue (not present, not activated or no driver)")
		raise SystemExit(1)
	else:
		print("...done [WIDTH="+str(vcap.get(cv.CAP_PROP_FRAME_WIDTH))+",HEIGTH="+str(vcap.get(cv.CAP_PROP_FRAME_HEIGHT))+",FPS="+str(vcap.get(cv.CAP_PROP_FPS))+"]")
time.sleep(2.0)


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#sock.settimeout(1.0)

framewidth = int(vcap.get(cv.CAP_PROP_FRAME_WIDTH))
frameheight = int(vcap.get(cv.CAP_PROP_FRAME_HEIGHT))

# 2019-10-28: Added
hostname = socket.gethostname()
testsock =  socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
testsock.connect((sockaddr))
ipaddr = testsock.getsockname()[0]
testsock.close()


while True:
	ret, imgCam = vcap.read()
	cv.imshow("Host: " + hostname + " (" + ipaddr + ") - Webcam", imgCam)

	print("sent ... ["+str(sockaddr)+" ## " +str(time.time())+"]")
	sockmsg = b"[##picture##start]"
	sock.sendto(sockmsg, sockaddr)

	ia = 0
	arrTrans = np.array(imgCam.copy()).flatten()
	imax = arrTrans.size
	while imax > ia:
		sock.sendto(arrTrans[ia:ia+1472], sockaddr)
		ia = ia + 1472

	sockmsg = b"[##picture##end]"+imax.to_bytes(4, byteorder="big")+framewidth.to_bytes(4, byteorder="big")+frameheight.to_bytes(4, byteorder="big")
	sock.sendto(sockmsg, sockaddr)
	print("... done ["+str(imax)+" ## " +str(time.time())+"]")
	time.sleep(0.1)

	key = cv.waitKey(1) & 0xFF
	if key == ord("q"):
		break


vcap.release()
cv.destroyAllWindows()
