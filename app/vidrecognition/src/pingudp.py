###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:			vidrecognition/src/pingudp.py
#
# Description:	UDP ping script
#
# Author:		Florian Kemser
#				florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#				Faculty of Electrical and Computer Engineering
#				Institute of Communication Technology
#				Deutsche Telekom Chair of Communication Networks
#
# Date:			Aug 2019 - Jan 2020
###########################################################################################

import time
import socket
import sys

sockaddr = (str(sys.argv[1]), int(sys.argv[2]))
interval = float(sys.argv[3])

print("UDP dummy sender started, destination " + str(sockaddr) + " , interval " + str(interval) + " s")

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)

i = 0
while True:
	sock.sendto(str(i).encode(), sockaddr)
	i+=1
	time.sleep(interval)
