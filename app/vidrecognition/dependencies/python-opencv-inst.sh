#!/bin/sh

###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:		vidrecognition/dependencies/python-opencv-inst.sh
#
# Description:	Installation script for dependencies
#		related to the video recognition application
#
# Author:	Florian Kemser
#		florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#		Faculty of Electrical and Computer Engineering
#		Institute of Communication Technology
#		Deutsche Telekom Chair of Communication Networks
#
# Date:		Aug 2019 - Mar 2020
###########################################################################################

###########################################################################################
######################################## CONSTANTS ########################################
###########################################################################################

#################################### Default Arguments ####################################
# Identifier		Parameter		Description
# -----------------------------------------------------------------------------------------
# ARG_DIST		-d, --dist <value>	distribution to install dependencies for

LIST_ARGS="ARG_DIST" 	# all arguments

ARG_DIST=""		# Default
###########################################################################################

########################################## Others #########################################
VER_ALPINE=3.10
VER_OPENCV=3.4.7
URL_OPENCV="https://github.com/opencv/opencv/archive/$VER_OPENCV.tar.gz"
###########################################################################################

################################### Packages (MEDIA_IO) ###################################
# MEDIA_IO_LIB_ALPINE="jasper-libs libjpeg-turbo libpng libwebp openexr tiff"
# MEDIA_IO_LIB_UBUNTU="libjasper1 libjpeg9 libpng12-0 libwebp5 libopenexr22 libtiff5"

# MEDIA_IO_DEV_ALPINE="jasper-dev libjpeg-turbo-dev libpng-dev libwebp-dev openexr-dev tiff-dev"
# MEDIA_IO_DEV_UBUNTU="libjasper-dev libjpeg9-dev libpng12-dev libwebp-dev libopenexr-dev libtiff5-dev"
###########################################################################################

################################### Packages (VIDEO_IO) ###################################
# VIDEO_IO_LIB_ALPINE="ffmpeg-libs (gstreamer gst-plugins-base) libavc1394 libdc1394 libgphoto2 v4l-utils-libs"
# VIDEO_IO_LIB_UBUNTU=""

# VIDEO_IO_DEV_ALPINE="ffmpeg-dev (gstreamer-dev gst-plugins-base-dev) libavc1394-dev libdc1394-dev libgphoto2-dev v4l-utils-dev"
# VIDEO_IO_DEV_UBUNTU="(libavcodec-dev libavformat-dev libswscale-dev) (libgstreamermm-1.0-dev libgstreamer-plugins-base1.0-dev) libavc1394-dev libdc1394-22-dev libgphoto2-dev"
###########################################################################################

##################################### Packages (BUILD) ####################################
BUILD_ALPINE="build-base clang clang-dev cmake pkgconf wget"
BUILD_UBUNTU="build-essential cmake pkg-config"
###########################################################################################

##################################### Packages (OTHER) ####################################
OTHER_ALPINE="python3 py-numpy gtk+2.0 libstdc++"
OTHER_UBUNTU="python3 python3-numpy libgtk2.0-0 libstdc++"

OTHER_DEV_ALPINE="python3-dev py-numpy-dev gtk+2.0-dev"
OTHER_DEV_UBUNTU="python3-dev libgtk2.0-dev"
###########################################################################################

##################################### Packages (TOOLS) ####################################
TOOLS_ALPINE="stress-ng"
TOOLS_UBUNTU="stress-ng"
###########################################################################################

######################################## Commands #########################################
CMD_PKG_ADD_ALPINE_1="apk add --update --no-cache --virtual build-dependencies $BUILD_ALPINE $MEDIA_IO_DEV_ALPINE $VIDEO_IO_DEV_ALPINE $OTHER_DEV_ALPINE"
CMD_PKG_ADD_ALPINE_2="apk add --update --no-cache $MEDIA_IO_LIB_ALPINE $VIDEO_IO_LIB_ALPINE $OTHER_ALPINE $TOOLS_ALPINE"
CMD_PKG_ADD_UBUNTU_1="sudo apt-get update && \
sudo apt-get install $BUILD_UBUNTU $MEDIA_IO_DEV_UBUNTU $MEDIA_IO_LIB_UBUNTU $VIDEO_IO_DEV_UBUNTU $VIDEO_IO_LIB_UBUNTU $OTHER_DEV_UBUNTU $OTHER_UBUNTU"

CMD_PKG_DEL_ALPINE="apk del --purge build-dependencies"
CMD_PKG_DEL_UBUNTU="sudo apt-get --purge $BUILD_UBUNTU $MEDIA_IO_DEV_UBUNTU $VIDEO_IO_DEV_UBUNTU $OTHER_DEV_UBUNTU && sudo apt-get clean"

CMD_REPO_ALPINE="echo -e \"\n\
http://dl-cdn.alpinelinux.org/alpine/v$VER_ALPINE/main\n\
http://dl-cdn.alpinelinux.org/alpine/v$VER_ALPINE/community\" >> /etc/apk/repositories"
CMD_REPO_UBUNTU=""
###########################################################################################

########################################## CMake ##########################################
CMAKE_BUILD="\
-D BUILD_DOCS=OFF \
-D BUILD_EXAMPLES=OFF \
-D BUILD_JAVA=OFF \
-D BUILD_PERF_TESTS=OFF \
-D BUILD_TESTS=OFF"

CMAKE_CMAKE="\
-D CMAKE_BUILD_TYPE=RELEASE \
-D CMAKE_C_COMPILER=/usr/bin/clang \
-D CMAKE_CXX_COMPILER=/usr/bin/clang++ \
-D CMAKE_INSTALL_PREFIX=/usr/local"

CMAKE_INSTALL="\
-D INSTALL_C_EXAMPLES=OFF \
-D INSTALL_PYTHON_EXAMPLES=OFF"

CMAKE_PYTHON="\"\
-D PYTHON_DEFAULT_EXECUTABLE=/usr/bin/python3 \
-D PYTHON3_PACKAGES_PATH=\$(python3 -c \"from distutils.sysconfig import get_python_lib; print(get_python_lib())\")\""

CMAKE_GUI="\
-D WITH_GTK=ON \
-D WITH_GTK_2_X=OFF \
-D WITH_VTK=OFF"

CMAKE_MEDIA_IO="\
-D WITH_1394=OFF \
-D WITH_JASPER=OFF \
-D WITH_IMGCODEC_HDR=OFF \
-D WITH_IMGCODEC_PXM=OFF \
-D WITH_IMGCODEC_SUNRASTER=OFF \
-D WITH_JPEG=OFF \
-D WITH_OPENEXR=OFF \
-D WITH_PNG=OFF \
-D WITH_QUIRC=OFF \
-D WITH_TIFF=OFF \
-D WITH_WEBP=OFF"

CMAKE_VIDEO_IO="\
-D WITH_FFMPEG=OFF \
-D WITH_GPHOTO2=OFF \
-D WITH_GSTREAMER=OFF \
-D WITH_LIBV4L=ON \
-D WITH_V4L=ON"

CMAKE_PARALLEL_FRAMEWORK="-D WITH_PTHREADS_PF=OFF"

CMAKE_SUPPORT_TRACE="-D WITH_ITT=OFF"

CMAKE_OTHER="-D WITH_EIGEN=OFF \
-D WITH_IPP=OFF \
-D WITH_LAPACK=OFF \
-D WITH_PROTOBUF=OFF \
-D WITH_TBB=OFF"

CMAKE_OPENCL="-D WITH_OPENCL=OFF \
-D WITH_OPENCLAMDBLAS=OFF \
-D WITH_OPENCLAMDFFT=OFF \
-D WITH_OPENCL_SVM=OFF"
###########################################################################################

###########################################################################################
######################################## VARIABLES ########################################
###########################################################################################
cmd_pkg_add_1="" # 1st command to add packages
cmd_pkg_add_2="" # 2nd command to add packages
cmd_pkg_del=""	# Command to remove packages
cmd_repo=""	# Command to add additional repositories
###########################################################################################

###########################################################################################
######################################## FUNCTIONS ########################################
###########################################################################################

###########################################################################################
printUsage() {
	local scriptname="$(basename $0)"
	cat <<EOF

Usage: $scriptname -d [alpine|ubuntu] ...

Installs application-related dependencies.

Options:
	-h, --help				show this help message and exit

	-d, --dist DIST				distribution
						[alpine|ubuntu]

Examples:
	$scriptname -d alpine
	$scriptname -d ubuntu

EOF
}
###########################################################################################

###########################################################################################
########################################## MAIN ###########################################
###########################################################################################
while [ $# -gt 0 ]
do
	case $1 in
		-h|--help)
			printUsage
			exit 0
			;;
		-d|--dist)
			ARG_DIST="$2"
			case $ARG_DIST in
				alpine)
					cmd_pkg_add_1="$CMD_PKG_ADD_ALPINE_1"
					cmd_pkg_add_2="$CMD_PKG_ADD_ALPINE_2"
					cmd_pkg_del="$CMD_PKG_DEL_ALPINE"
					cmd_repo="$CMD_REPO_ALPINE"
					;;
				ubuntu)
					cmd_pkg_add_1="$CMD_PKG_ADD_UBUNTU_1"
					cmd_pkg_add_2=""
					cmd_pkg_del="$CMD_PKG_DEL_UBUNTU"
					cmd_repo="$CMD_REPO_UBUNTU"
					;;
				*)
					printUsage
					exit 1
					;;
			esac
			shift
			;;
		*)
			printUsage
			exit 1
			;;
	esac
	shift
done

if [ -z "$ARG_DIST" ]
then
	printUsage
	exit 1
fi

# Add repositories
$cmd_repo

# Install packages
$cmd_pkg_add_1
$cmd_pkg_add_2

# Get python path
eval CMAKE_PYTHON="$CMAKE_PYTHON"

# Symbolic Links
ln -s /usr/bin/python3 /usr/local/bin/python && \
ln -s /usr/bin/pip3 /usr/local/bin/pip && \
ln -s /usr/include/locale.h /usr/include/xlocale.h
# ln -s /usr/include/libpng16 /usr/include/libpng

# Install OpenCV
cd /tmp && \
cd "$(wget -qO- $URL_OPENCV | tar -xvz | tail -n1 | cut -f1 -d"/")" && \
mkdir build && cd build && \
cmake $CMAKE_BUILD $CMAKE_CMAKE $CMAKE_INSTALL $CMAKE_PYTHON $CMAKE_GUI $CMAKE_MEDIA_IO $CMAKE_VIDEO_IO $CMAKE_PARALLEL_FRAMEWORK $CMAKE_SUPPORT_TRACE $CMAKE_OTHER $CMAKE_OPENCL .. && \
make -j$(nproc) && make install

# Cleanup
cd / && rm -vrf /tmp/opencv* && $cmd_pkg_del
