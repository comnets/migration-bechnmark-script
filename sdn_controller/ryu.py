###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:			sdn_controller/ryu.sh
#
# Description:	Ryu SDN Controller Application
#
# Author:		Florian Kemser
#				florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#				Faculty of Electrical and Computer Engineering
#				Institute of Communication Technology
#				Deutsche Telekom Chair of Communication Networks
#
# Date:			Aug 2019 - Jan 2020
###########################################################################################

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types

# BEGIN Modification container/VM migration
from ryu.lib import mac
from ryu.lib.packet import arp
from ryu.lib.packet import in_proto
from ryu.lib.packet import ipv4
from ryu.lib.packet import tcp
from ryu.lib.packet import vlan
from datetime import datetime
# END Modification container/VM migration

class SimpleSwitch13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    # BEGIN Modification container/VM migration
    VHOST_MAC = '${VHOST_MAC}'
    VHOST_IP = '${VHOST_IP}'
    OF_COOKIE_MIG = ${OF_COOKIE_MIG}
    OF_COOKIE_REDIR = ${OF_COOKIE_REDIR}
    OF_TABLE_HW = ${OF_TABLE_HW}
    OF_TABLE_SW = ${OF_TABLE_SW}
    MIG_TYPE = '${MIG_TYPE}'
    # END Modification container/VM migration

    def __init__(self, *args, **kwargs):
        super(SimpleSwitch13, self).__init__(*args, **kwargs)
        self.mac_to_port = {}

        # BEGIN Modification container/VM migration
        self.mig_src_mac = '${HOST1_MAC}'
        self.mig_src_out = ${HOST1_OUT}
        self.mig_dst_mac = '${HOST2_MAC}'
        self.mig_dst_out = ${HOST2_OUT}
        self.mig_port = ${PORT_MIG}
        self.mig_port_reset = ${PORT_MIG_RESET}

        self.mig_match_hw = ''
        self.mig_match_hw_next = ''
        self.mig_instruction_hw = ''
        self.mig_match_sw = ''
        self.mig_match_sw_next = ''
        self.mig_instruction_sendto_controller = ''

        self.mig_match_redir = ''
        self.mig_instruction_redir = ''
        self.mig_instruction_redir_next = ''
        # END Modification container/VM migration

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, self.toInstructionActions(datapath, actions))

        # BEGIN Modification container/VM migration

        # Delete flows between host1 and host2
        self.del_flow_match(datapath,parser.OFPMatch(eth_src=(self.mig_src_mac), eth_dst=(self.mig_dst_mac)))
        self.del_flow_match(datapath,parser.OFPMatch(eth_src=(self.mig_dst_mac), eth_dst=(self.mig_src_mac)))

        # Delete VM/container related flows
        self.del_flow_match(datapath,parser.OFPMatch(eth_dst=(SimpleSwitch13.VHOST_MAC)))
        self.del_flow_match(datapath,parser.OFPMatch(eth_src=(SimpleSwitch13.VHOST_MAC)))

        ## Initial flow for migration detection (hardware table -> software table)

        # Definition Instructions
        self.mig_instruction_sendto_controller =  self.toInstructionActions(datapath, [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER)])

        if (SimpleSwitch13.MIG_TYPE == "docker"):
            # Initial flow for migration detection (hardware table -> controller)
            self.mig_match_hw = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP, eth_src=self.mig_src_mac, eth_dst=self.mig_dst_mac, ip_proto=6, tcp_dst=self.mig_port)
            self.mig_match_hw_next = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP, eth_src=self.mig_dst_mac, eth_dst=self.mig_src_mac, ip_proto=6, tcp_dst=self.mig_port_reset)
            self.mig_instruction_hw = self.mig_instruction_sendto_controller

        elif (SimpleSwitch13.MIG_TYPE == "kvm"):
            # Initial flow for migration detection (hardware table -> software table)
            self.mig_match_hw = parser.OFPMatch(in_port=self.mig_dst_out, eth_type=ether_types.ETH_TYPE_ARP, eth_src=SimpleSwitch13.VHOST_MAC, eth_dst=mac.BROADCAST_STR)
            self.mig_match_hw_next = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP, eth_src=self.mig_dst_mac, eth_dst=self.mig_src_mac, ip_proto=6, tcp_dst=(self.mig_port_reset))
            # self.mig_match_hw_next = parser.OFPMatch(in_port=self.mig_src_out, eth_type=ether_types.ETH_TYPE_ARP, eth_src=SimpleSwitch13.VHOST_MAC, eth_dst=mac.BROADCAST_STR)
            self.mig_instruction_hw = [parser.OFPInstructionGotoTable(SimpleSwitch13.OF_TABLE_SW)]

            # Initial flow for migration detection (software table -> controller)
            self.mig_match_sw = parser.OFPMatch(in_port=self.mig_dst_out, eth_type=ether_types.ETH_TYPE_ARP, eth_src=SimpleSwitch13.VHOST_MAC, eth_dst=mac.BROADCAST_STR, arp_op=arp.ARP_REQUEST, arp_spa=SimpleSwitch13.VHOST_IP, arp_tpa=SimpleSwitch13.VHOST_IP, arp_sha=SimpleSwitch13.VHOST_MAC, arp_tha=mac.DONTCARE_STR)
            # self.mig_match_sw_next = parser.OFPMatch(in_port=self.mig_src_out, eth_type=ether_types.ETH_TYPE_ARP, eth_src=SimpleSwitch13.VHOST_MAC, eth_dst=mac.BROADCAST_STR, arp_op=arp.ARP_REQUEST, arp_spa=SimpleSwitch13.VHOST_IP, arp_tpa=SimpleSwitch13.VHOST_IP, arp_sha=SimpleSwitch13.VHOST_MAC, arp_tha=mac.DONTCARE_STR)
            self.mig_match_sw_next = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP, eth_src=self.mig_dst_mac, eth_dst=self.mig_src_mac, ip_proto=6, tcp_dst=(self.mig_port_reset))
            self.add_flow(datapath, 10, self.mig_match_sw, self.mig_instruction_sendto_controller,'',SimpleSwitch13.OF_COOKIE_MIG,SimpleSwitch13.OF_TABLE_SW)

        # Add flow to HW table
        self.add_flow(datapath, 10, self.mig_match_hw, self.mig_instruction_hw,'',SimpleSwitch13.OF_COOKIE_MIG,SimpleSwitch13.OF_TABLE_HW)

        # Initial settings for container/VM traffic redirection
        self.mig_match_redir = parser.OFPMatch(eth_dst=(SimpleSwitch13.VHOST_MAC))
        self.mig_instruction_redir = self.toInstructionActions(datapath, [parser.OFPActionOutput(self.mig_src_out)])
        self.mig_instruction_redir_next = self.toInstructionActions(datapath, [parser.OFPActionOutput(self.mig_dst_out)])

        print("{0}  Switch initialised.".format(datetime.timestamp(datetime.now())))
        # END Modification container/VM migration

    def add_flow(self, datapath, priority, match, instructions, buffer_id=None, cookie=0, table_id=${OF_TABLE_HW}):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=instructions, table_id=table_id, cookie=cookie)
        else:
           mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                        match=match, instructions=instructions, table_id=table_id, cookie=cookie)
        datapath.send_msg(mod)

    def toInstructionActions(self, datapath, actions):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        return [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]

    def del_flow_cookie(self, datapath, cookie):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        mod = parser.OFPFlowMod(datapath=datapath, match=parser.OFPMatch(), table_id=ofproto.OFPTT_ALL, command=ofproto.OFPFC_DELETE, out_port=ofproto.OFPP_ANY, out_group=ofproto.OFPG_ANY, cookie=cookie, cookie_mask=0xFFFFFFFFFFFFFFFF)
        datapath.send_msg(mod)

    def del_flow_match(self, datapath, match):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        mod = parser.OFPFlowMod(datapath=datapath, match=match, table_id=ofproto.OFPTT_ALL, command=ofproto.OFPFC_DELETE, out_port=ofproto.OFPP_ANY, out_group=ofproto.OFPG_ANY)
        datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return
        dst = eth.dst
        src = eth.src

        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

        self.logger.info("packet in (dpid src dst in_port eth_type cookie): %s %s %s %s %s %s", dpid, src, dst, in_port, eth.ethertype, msg.cookie)

        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        actions = [parser.OFPActionOutput(out_port)]

        # BEGIN Modification container/VM migration
        if (msg.cookie == SimpleSwitch13.OF_COOKIE_MIG):
            self.logger.info("%s  Migration detected  Source: %s Destination: %s",datetime.timestamp(datetime.now()),src,dst)

            # Modify flow for VM/container traffic redirection
            self.del_flow_cookie(datapath, SimpleSwitch13.OF_COOKIE_REDIR)
            self.add_flow(datapath, 10, self.mig_match_redir, self.mig_instruction_redir_next,'', SimpleSwitch13.OF_COOKIE_REDIR, SimpleSwitch13.OF_TABLE_HW)
            tmp_instruction = self.mig_instruction_redir_next
            self.mig_instruction_redir_next = self.mig_instruction_redir
            self.mig_instruction_redir = tmp_instruction

            # Modify flow for migration detection
            self.del_flow_cookie(datapath, SimpleSwitch13.OF_COOKIE_MIG)
            self.add_flow(datapath, 10, self.mig_match_hw_next, self.mig_instruction_hw, '', SimpleSwitch13.OF_COOKIE_MIG, SimpleSwitch13.OF_TABLE_HW)

            if (SimpleSwitch13.MIG_TYPE == "kvm"):
                self.add_flow(datapath, 10, self.mig_match_sw_next, self.mig_instruction_sendto_controller, '', SimpleSwitch13.OF_COOKIE_MIG, SimpleSwitch13.OF_TABLE_SW)
                tmp_match = self.mig_match_sw_next
                self.mig_match_sw_next = self.mig_match_sw
                self.mig_match_sw = tmp_match

            tmp_match = self.mig_match_hw_next
            self.mig_match_hw_next = self.mig_match_hw
            self.mig_match_hw = tmp_match

        # END Modification container/VM migration


        # install a flow to avoid packet_in next time
        if out_port != ofproto.OFPP_FLOOD:
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst, eth_src=src)
            # verify if we have a valid buffer_id, if yes avoid to send both
            # flow_mod & packet_out
            if msg.buffer_id != ofproto.OFP_NO_BUFFER:
                self.add_flow(datapath, 1, match, self.toInstructionActions(datapath, actions), msg.buffer_id)
                return
            else:
                self.add_flow(datapath, 1, match, self.toInstructionActions(datapath, actions))
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
                data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                in_port=in_port, actions=actions, data=data)
        datapath.send_msg(out)
