#!/bin/sh

###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:		src/dockerBuild.sh
#
# Description:	Docker Image Build Script
#
# Author:	Florian Kemser
#		florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#		Faculty of Electrical and Computer Engineering
#		Institute of Communication Technology
#		Deutsche Telekom Chair of Communication Networks
#
# Date:		Aug 2019 - Mar 2020
###########################################################################################

# APP_NAME="alpine-py-opencv"
# APP_NAME="vidrecognition"
USER="YOUR_USERNAME"

DIR_ROOT="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
APP_DIR="$DIR_ROOT/../app/$APP_NAME"

sudo docker login
sudo docker build --tag="$APP_NAME" --file="$APP_DIR/docker/Dockerfile" "$APP_DIR/src"
sudo docker tag $APP_NAME:latest $USER/${APP_NAME}:latest
sudo docker push $USER/${APP_NAME}:latest
