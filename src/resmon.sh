#!/bin/bash

###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:		src/resmon.sh
#
# Description:	Resource Monitoring (CPU, RAM, network I/O) Script
#
# Author:	Florian Kemser
#		florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#		Faculty of Electrical and Computer Engineering
#		Institute of Communication Technology
#		Deutsche Telekom Chair of Communication Networks
#
# Date:		Aug 2019 - Jan 2020
###########################################################################################

###########################################################################################
######################################## ATTENTION ########################################
###########################################################################################
# This script has to be executed with sudo rights.

###########################################################################################
######################################## CONSTANTS ########################################
###########################################################################################

#################################### Default Arguments ####################################
ARG_CPUACCT_DEF=""
ARG_FILE_DEF=""
ARG_INTERFACE_DEF=""
ARG_INTERVAL_DEF="0.5"
ARG_NAME_DEF=""
ARG_TYPE_DEF=""

######################################### Commands #########################################
cpu_cores="$(nproc --all)"
cmd_host_rx="\"cat /sys/class/net/\${ARG_INTERFACE}/statistics/rx_bytes\""
cmd_host_rx_err="\"cat /sys/class/net/\${ARG_INTERFACE}/statistics/rx_errors\""
cmd_host_rx_drop="\"cat /sys/class/net/\${ARG_INTERFACE}/statistics/rx_dropped\""
cmd_host_tx="\"cat /sys/class/net/\${ARG_INTERFACE}/statistics/tx_bytes\""
cmd_host_tx_err="\"cat /sys/class/net/\${ARG_INTERFACE}/statistics/tx_errors\""
cmd_host_tx_drop="\"cat /sys/class/net/\${ARG_INTERFACE}/statistics/tx_dropped\""

###########################################################################################
######################################## FUNCTIONS ########################################
###########################################################################################

######################################## printUsage #######################################
# Prints user information to the terminal
#
# Requires:
printUsage() {
	local scriptname="$(basename $0)"
	cat <<EOF

Usage: $scriptname [OPTION]... [MEASUREMENT_FILE]

Monitors the host's and the container's/VM's CPU, RAM, and network I/O usage
and saves the results in [MEASUREMENT_FILE].

Options:
	-h, --help				show this help message and exit

	-c, --cpuacct FILE			CPU accounting controller file

	-i, --interface IFACE			physical network interface (SDN data network)

	-n, --name NAME				name of the container/VM

     	-t, --type TYPE				virtualization type [docker|kvm]

	-v, --interval TIME			monitoring interval (in s)
		 				(default: $ARG_INTERVAL_DEF)

Examples:
	$scriptname -i enp1s0f1 -n virt -t docker
	$scriptname -i enp1s0f1 -n virt -t docker -v 0.25 -c "/tmp/cpuacct.usage_percpu"

EOF
}
###########################################################################################

####################################### checkParams #######################################
# Check if all needed parameters are defined
#
# Requires:	ARG_FILE ARG_INTERFACE ARG_NAME ARG_TYPE
#
# Sets:		ARG_FILE ARG_INTERFACE ARG_NAME ARG_TYPE
checkParams() {
	for var in ARG_FILE ARG_INTERFACE ARG_NAME ARG_TYPE
	do
		eval val="\$${var}"
		if [ -z "$val" ]
		then
			printUsage
			exit 1
		fi
	done
}
###########################################################################################

####################################### setDefaults #######################################
# Initialises the constants / variables with its default values
#
# Requires:	ARG_CPUACCT ARG_FILE ARG_INTERFACE ARG_INTERVAL ARG_NAME ARG_TYPE
#
# Sets:		ARG_INTERVAL
setDefaults() {
	# Constants
	for var in ARG_CPUACCT ARG_FILE ARG_INTERFACE ARG_INTERVAL ARG_NAME ARG_TYPE
	do
		eval $var="\$${var}_DEF"
	done
}
###########################################################################################

######################################## timestamp ########################################
# Creates a timestamp
#
# Returns:	Current time as an Epoch timestamp (in ns)
timestamp() {
	timestamp=$(date +%s%N)
	echo $timestamp
}
###########################################################################################


###################################### updateGlobals ######################################
# Sets constants and variables depending on the passed arguments
#
# Requires: 	cmd_host_rx cmd_host_rx_err cmd_host_rx_drop
#		cmd_host_tx cmd_host_tx_err cmd_host_tx_drop
#
# Sets: 	cmd_host_rx cmd_host_rx_err cmd_host_rx_drop
#		cmd_host_tx cmd_host_tx_err cmd_host_tx_drop
updateGlobals() {
	for const in cmd_host_rx cmd_host_rx_err cmd_host_rx_drop cmd_host_tx cmd_host_tx_err cmd_host_tx_drop
	do
		eval eval $const="\$$const"
	done
}
###########################################################################################

###########################################################################################
########################################## MAIN ###########################################
###########################################################################################
# Set default values
setDefaults

# Process passed arguments
while [ $# -gt 0 ]
do
	case $1 in
		-h|--help)
			printUsage
			exit 0
			;;
		-c|--cpuacct)
			ARG_CPUACCT="$2"
			shift
			;;
		-i|--interface)
			ARG_INTERFACE="$2"
			ifconfig "$ARG_INTERFACE" >/dev/null 2>&1
			if [ $? -ne 0 ]
			then
				printUsage
				exit 1
			fi
			shift
			;;
		-n|--name)
			ARG_NAME="$2"
			shift
			;;
		-t|--type)
			ARG_TYPE="$2"
			case $ARG_TYPE in
				docker|kvm)
					;;
				*)
					printUsage
					exit 1
					;;
			esac
			shift
			;;
		-v|--interval)
			ARG_INTERVAL="$2"
			shift
			;;
		*)
			[ $# -ge 1 ] && touch "$@" >/dev/null 2>&1
			if [ $? -eq 0 ]
			then
				ARG_FILE="$@"
			else
				printUsage
				exit 1
			fi
			;;
	esac
	shift
done

checkParams
updateGlobals

case "$ARG_TYPE" in
	docker)
		if ! [ -z "$ARG_CPUACCT" ]
		then
			cmd_cpu="cat $ARG_CPUACCT"
		else
			cmd_cpu="cat /sys/fs/cgroup/cpu/docker/\$docker_cid/cpuacct.usage_percpu"
		fi
		cmd_mem="cat /sys/fs/cgroup/memory/docker/\$docker_cid/memory.stat"
		cmd_check="docker ps | grep $ARG_NAME"

		# Needed for container network metrics
		# See: https://docs.docker.com/config/containers/runmetrics/#network-metrics
		mkdir -p /var/run/netns
		docker_cmd_pid_1="head -n 1 /sys/fs/cgroup/devices/docker/\$docker_cid/tasks"
		docker_cmd_syml="ln -sf /proc/\$docker_pid_1/ns/net /var/run/netns/$ARG_NAME"
		cmd_virt="ip netns exec $ARG_NAME ifconfig eth0"
		;;

  	kvm)
		# cmd_cpu="virsh domstats --cpu-total $ARG_NAME | grep 'cpu.time' | cut -d '=' -f2"
		cmd_cpu="cat /sys/fs/cgroup/cpu/machine/\${ARG_NAME}.libvirt-qemu/cpuacct.usage_percpu"
		cmd_mem="virsh dommemstat $ARG_NAME"
		cmd_check="virsh list | grep $ARG_NAME"
		cmd_virt="virsh domstats $ARG_NAME | grep 'net.0'"
		;;

esac

# CSV header
i=2
cpu_header="t_cpu_1"
cpu_header_units="ns"
cpu_dummy="-"
while [ $i -le $cpu_cores ]
do
	cpu_header="${cpu_header};t_cpu_$i"
	cpu_header_units="${cpu_header_units};ns"
  	cpu_dummy="${cpu_dummy};-"
	i=$(expr $i + 1)
done

echo -ne "t;${cpu_header};mem;virt_rx;virt_rx_err;virt_rx_drop;virt_tx;virt_tx_err;virt_tx_drop;host_rx;host_rx_err;host_rx_drop;host_tx;host_tx_err;host_tx_drop\n" >> "$ARG_FILE"
echo -ne "ns;${cpu_header_units};B;B;;;B;;;B;;;B;;\n" >> "$ARG_FILE"

while true
do

	if  [ "$(eval $cmd_check)" ]
  	then
    	{
      		if [ "$ARG_TYPE" = "docker" ]
      		then {
        		docker_cid="$(docker ps --no-trunc | grep $ARG_NAME | cut -d ' ' -f1)"
			docker_pid_1="$(eval $docker_cmd_pid_1)"
			eval $docker_cmd_syml
      		}
      		fi

		t=$(timestamp)
		cpu="$(eval $cmd_cpu)"
		mem="$(eval $cmd_mem)"
		virt="$(eval $cmd_virt)"
		host_rx="$($cmd_host_rx)"
		host_rx_err="$($cmd_host_rx_err)"
		host_rx_drop="$($cmd_host_rx_drop)"
		host_tx="$($cmd_host_tx)"
		host_tx_err="$($cmd_host_tx_err)"
		host_tx_drop="$($cmd_host_tx_drop)"

		case "$ARG_TYPE" in
			docker)
				cpu="$(echo "$cpu" | sed 's/ /;/g' | sed 's/;$//g')"

				mem_cache="$(echo "$mem" | grep 'total_cache ' | cut -d ' ' -f2)"
				mem_rss="$(echo "$mem" | grep 'total_rss ' | cut -d ' ' -f2)"
				mem="$(expr $mem_cache + $mem_rss)"

				virt_bytes="$(echo "$virt" | grep 'RX bytes:' | tr -s ' ')"
				virt_pkt_rx="$(echo "$virt" | grep 'RX packets:' | tr -s ' ')"
				virt_pkt_tx="$(echo "$virt" | grep 'TX packets:' | tr -s ' ')"

				virt_rx="$(echo "$virt_bytes" | cut -d ' ' -f3 | cut -d ':' -f2)"
				virt_rx_err="$(echo "$virt_pkt_rx" | cut -d ' ' -f4 | cut -d ':' -f2)"
				virt_rx_drop="$(echo "$virt_pkt_rx" | cut -d ' ' -f5 | cut -d ':' -f2)"

				virt_tx="$(echo "$virt_bytes" | cut -d ' ' -f7 | cut -d ':' -f2)"
				virt_tx_err="$(echo "$virt_pkt_tx" | cut -d ' ' -f4 | cut -d ':' -f2)"
				virt_tx_drop="$(echo "$virt_pkt_tx" | cut -d ' ' -f5 | cut -d ':' -f2)"
				;;

		        kvm)
				cpu="$(echo "$cpu" | sed 's/ /;/g' | sed 's/;$//g')"

				mem_actual=$(echo "$mem" | grep 'actual' | cut -d ' ' -f2)
				mem_unused=$(echo "$mem" | grep 'unused' | cut -d ' ' -f2)
				mem="$(expr \( $mem_actual - $mem_unused \) \* 1000 )"

				virt_rx="$(echo "$virt" | grep 'rx.bytes' | cut -d '=' -f2)"
				virt_rx_err="$(echo "$virt" | grep 'rx.errs' | cut -d '=' -f2)"
				virt_rx_drop="$(echo "$virt" | grep 'rx.drop' | cut -d '=' -f2)"

				virt_tx="$(echo "$virt" | grep 'tx.bytes' | cut -d '=' -f2)"
				virt_tx_err="$(echo "$virt" | grep 'tx.errs' | cut -d '=' -f2)"
				virt_tx_drop="$(echo "$virt" | grep 'tx.drop' | cut -d '=' -f2)"
				;;

		esac

	}

	else
    	{
      		t=$(timestamp)
      		host_rx="$($cmd_host_rx)"
      		host_tx="$($cmd_host_tx)"
      		cpu="$cpu_dummy"

		for param in mem virt_rx virt_rx_err virt_rx_drop virt_tx virt_tx_err virt_tx_drop
		do
			eval $param="-"
		done

	}

  	fi

	echo -ne "$t;$cpu;$mem;$virt_rx;$virt_rx_err;$virt_rx_drop;$virt_tx;$virt_tx_err;$virt_tx_drop;$host_rx;$host_rx_err;$host_rx_drop;$host_tx;$host_tx_err;$host_tx_drop\n" >> "$ARG_FILE"
	sleep $ARG_INTERVAL

done
