#!/bin/bash

###########################################################################################
######################################### LICENSE #########################################
###########################################################################################
# This file is part of ComNets Live Migration Benchmark
#
# ComNets Live Migration Benchmark is free software:
# You can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# ComNets Live Migration Benchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
###########################################################################################

###########################################################################################
########################################## INFO ###########################################
###########################################################################################
# File:		src/run.sh
#
# Description:	Benchmark Script
#
# Author:	Florian Kemser
#		florian.kemser@mailbox.tu-dresden.de
#
# Institution:	Technische Universität Dresden
#		Faculty of Electrical and Computer Engineering
#		Institute of Communication Technology
#		Deutsche Telekom Chair of Communication Networks
#
# Date:		Aug 2019 - Jan 2020
###########################################################################################

###########################################################################################
######################################## ATTENTION ########################################
###########################################################################################
# To adapt the script to your testbed please use the configuration file run.sh.options
# and do not make any changes in this file unless you would like to bugfix or modify
# this script.

###########################################################################################
########################################## IMPORT #########################################
###########################################################################################
DIR_ROOT="$(cd -P -- "$(dirname -- "$0")" && pwd -P)/.."
source "$DIR_ROOT/src/run.sh.options"

###########################################################################################
######################################## CONSTANTS ########################################
###########################################################################################
# Please list the identifiers of all constants/variables that are dynamically
# expanded, depending on the passed arguments. Please keep the order in which
# they should be expanded.
LIST_EXP=""

#################################### Default Arguments ####################################
# Identifier		Parameter		Description
# -----------------------------------------------------------------------------------------
# ARG_APPNAME		-a, --app <val>		test application to use
# ARG_CPUS		-c, --cpus <val>	number of CPUs cores to use for the test
# ARG_DEDICATED		-d, --dedicated		dedicated CPU core mode
# ARG_DOWNTIME					treshold value (in ms) for downtime decision
# ARG_MEMORY		-m, --memory <val>	amout of memory dedicated to container/VM
# ARG_RUN		-r, --run <val>		number of test cycles
# ARG_TYPE		-t, --type <val>	virtualisation type
# ARG_STRESS_CPU	-C, --stress-cpu <val>	CPU stress (in percentage/core)
# ARG_STRESS_MEM	-M, --stress-mem <val>	memory stress (amount of memory stressed)
# ARG_STRESS_NET	-N, --stress-net <val>	network stress (same direction as migration)
# ARG_STRESS_NET_REV	-O, --stress-net-rev <val> network stress (opposite direction)

LIST_ARGS="ARG_APPNAME ARG_CPUS ARG_DEDICATED ARG_DOWNTIME ARG_MEMORY ARG_RUN ARG_TYPE ARG_STRESS_CPU ARG_STRESS_MEM ARG_STRESS_NET ARG_STRESS_NET_REV" # all arguments

# ARG_APPNAME		test application to use
ARG_APPNAME_DEF=""

# ARG_CPUS		number of CPUs cores to use for the test
ARG_CPUS_DEF=""
ARG_CPUS_MIN="1"
ARG_CPUS_MAX=""

# ARG_DEDICATED		dedicated CPU core mode
ARG_DEDICATED_DEF="0"

# ARG_DOWNTIME		treshold value (in ms) for deciding if container/VM is down
ARG_DOWNTIME_DEF="15"
ARG_DOWNTIME_MIN="10"
ARG_DOWNTIME_MAX=""

# ARG_MEMORY		amout of memory dedicated to container/VM
ARG_MEMORY_DEF="128m"
ARG_MEMORY_MIN="4000000"
ARG_MEMORY_MAX=""

# ARG_RUN		number of test cycles
ARG_RUN_DEF="500"
ARG_RUN_MIN="1"
ARG_RUN_MAX="1000"

# ARG_TYPE		virtualisation type
ARG_TYPE_DEF="docker"

# ARG_STRESS_CPU
#
# CPU stress (in percentage of total CPU time per each available core)
# In case of dedicated CPU mode, only the container/VM cores are stressed.
ARG_STRESS_CPU_DEF=""
ARG_STRESS_CPU_MIN="0"
ARG_STRESS_CPU_MAX="100"

# ARG_STRESS_MEM	memory stress (amount of memory stressed)
ARG_STRESS_MEM_DEF=""

# ARG_STRESS_NET	network stress (same direction as migration)
ARG_STRESS_NET_DEF=""

# ARG_STRESS_NET_REV	network stress (opposite direction)
ARG_STRESS_NET_REV_DEF=""
###########################################################################################

##################################### Files & Folders #####################################
# Repository files and folders
DIR_APP_ROOT="$DIR_ROOT/app"
DIR_APP="\"\${DIR_APP_ROOT}/\${ARG_APPNAME}\""
DIR_APP_KVM="\"\${DIR_APP}/kvm\""		# test application VM image
DIR_APP_SRC="\"\${DIR_APP}/src\""		# test application source folder (full path)
DIR_APP_SRC_FOLDERNAME="\"\$( echo \$DIR_APP_SRC | rev | cut -d'/' -f 1 | rev )\"" # test application source folder (name)
DIR_MEASUREMENTS="$DIR_ROOT/measurements" 	# measurement results
DIR_SDNCONTROLLER="$DIR_ROOT/sdn_controller" 	# SDN controller
DIR_SRC="$DIR_ROOT/src" 			# source folder
FILE_RESMON="resmon.sh" 			# resource (CPU, RAM, network I/O) monitoring script
FILE_APP_CONFIG="app.sh.options"		# test application configuration file (just file, no path)
FILE_APP_SETUP_CLIENT="\"\${DIR_APP}/src/app.setup.client.sh\""

# Measurement files and folders
DIR_NAME="\"\${ARG_APPNAME}_\${ARG_TYPE}_$(date +%Y-%m-%d_%H-%M-%S)\"" # default measurement folder (only name)
DIR_LOG="\"$DIR_MEASUREMENTS/\${DIR_NAME}\"" # default measurement folder (full path)
FILE_MEASUREMENTS="\"\${DIR_LOG}/\${DIR_NAME}_results.csv\"" # migration measurement CSV file
FILE_MEASUREMENTS_HEADER="t;r;c;d;m;D+;D-;R_CPU;R_RAM" # migration measurement CSV file - header
FILE_MEASUREMENTS_HEADER="\"${FILE_MEASUREMENTS_HEADER}\n\${ARG_TYPE};\${ARG_RUN};\${ARG_CPUS};\${ARG_DEDICATED};\${ARG_MEMORY};\${ARG_STRESS_NET};\${ARG_STRESS_NET_REV};\${ARG_STRESS_CPU};\${ARG_STRESS_MEM}\""
FILE_RESMON_H1="\"\${DIR_LOG}/\${DIR_NAME}_resmon_h1.csv\"" # resource measurements CSV host 1
FILE_RESMON_H2="\"\${DIR_LOG}/\${DIR_NAME}_resmon_h2.csv\"" # resource measurements CSV host 2
LOG_IPERF_CAP="\"\${DIR_LOG}/iperf_capacity.log\"" # iperf logfile network (idle) capacity measurement
LOG_IPERF_STRESS_H1H2="\"\${DIR_LOG}/iperf_stress_h1_h2.log\""
LOG_IPERF_STRESS_H2H1="\"\${DIR_LOG}/iperf_stress_h2_h1.log\""

# Temporary files and folders (on host1, host2, controller)
DIR_TMP="/tmp/migration" # temporary folder
FILE_TMP_DUMMY="$DIR_TMP/dummy.file" # dummy file used for re-migration
FILE_TMP_RESMON="$DIR_TMP/resmon.csv" # resource measurement CSV file
LOG_TMP_IPERF="$DIR_TMP/iperf.log" # iperf logfile for network stress (on host1/2)
LOG_TMP_TCPDUMP="$DIR_TMP/tcpdump.log" # tcpdump logfile (on host1/2)
###########################################################################################

######################################### Host 1 ##########################################
HOST1_SSH_DATA="${HOST1_USER}@${HOST1_IP_DATA}" 		# SSH URI (SDN Data)
HOST1_SSH_MANAGEMENT="${HOST1_USER}@${HOST1_IP_MANAGEMENT}" 	# SSH URI (Management)
###########################################################################################

######################################### Host 2 ##########################################
HOST2_SSH_DATA="${HOST2_USER}@${HOST2_IP_DATA}"			# SSH URI (SDN Data)
HOST2_SSH_MANAGEMENT="${HOST2_USER}@${HOST2_IP_MANAGEMENT}"	# SSH URI (Management)
###########################################################################################

###################################### SDN Controller #####################################
CONTROLLER_LOG="$DIR_TMP/controller.log" 		# logfile
CONTROLLER_SSH="${CONTROLLER_USER}@${CONTROLLER_IP}" 	# SSH URI
###########################################################################################

###################################### Docker & KVM #######################################
DOCKER_KVM_LOG="$DIR_TMP/virt.log" 			# logfile of container/VM
###########################################################################################

######################################### Docker ##########################################
# General
DOCKER_CSV_HEADER="T_m;T_chk;T_cpy;T_start;M;T_d"		# CSV header (columns)
DOCKER_CSV_HEADER="${DOCKER_CSV_HEADER}\nms;ms;ms;ms;B;ms"	# CSV header (units)

# Container related
DOCKER_CT_CHECKPOINT_NAME="checkpoint"				# checkpoint name
DOCKER_CT_CHECKPOINT_DIR="$DIR_TMP/$DOCKER_CT_CHECKPOINT_NAME"	# checkpoint directory
###########################################################################################

######################################## KVM/QEMU #########################################
# General
KVM_CSV_HEADER="T_m;T_precpy;T_copy;T_start;M;mem_app;T_d"	# CSV header (columns)
KVM_CSV_HEADER="${KVM_CSV_HEADER}\nms;ms;ms;ms;B;B;ms"		# CSV header (units)
KVM_LOG="$DIR_TMP/kvm.log" 					# logifle KVM/QEMU monitor

# VM related
KVM_VM_VDISK="$DIR_TMP/${DOCKER_KVM_NAME}.tmp.qcow2" 		# VM virtual disk file
KVM_VM_SSH="\"\${APP_KVM_VM_USER}@${DOCKER_KVM_IP}\""		# SSH URI
###########################################################################################

###########################################################################################
######################################## FUNCTIONS ########################################
###########################################################################################

################################ checkParam "$1" "$2" "$3" ################################
# Checks if a given argument fits its allowed range of values
#
# Arguments:	1	variable name
#			without '$'
#
#		2	minimum (optional)
#			smallest unit (e.g. Bytes), just number, no unit
#
#		3	maximum (optional)
#			smallest unit (e.g. Bytes), just number, no unit
#
# Return Code:
#		0	success, variable/value within allowed ranges
#		1	error, value not within allowed ranges
#		2	error, value has a wrong unit
#		3	error, unknown variable
#
# Example: 	checkParam "ARG_RUN" "1" "100"
#		checkParam "ARG_MEMORY" "4000000" "" -> min 4 MB, no max
#		checkParam "ARG_MEMORY" "" "256000000" -> no min, max 256 MB
checkParam() {
	local ARG_VAR="$1"
	local ARG_MIN="$2"
	local ARG_MAX="$3"

	eval local ARG_VALUE="\$$ARG_VAR"

	local num=""
	local unit=""
	local fact=""

	if ! [ $ARG_VALUE -le 0 -o $ARG_VALUE -gt 0 ] 2>/dev/null
	then
	  	# num="$(echo $ARG_VALUE | rev | cut -c 2- | rev)"
		unit="$(echo $ARG_VALUE | rev | cut -c 1)"

		case $ARG_VAR in
			ARG_MEMORY|ARG_STRESS_MEM)
				case $unit in
					b|k|m|g)
						;;
					*)
						return 2
						;;
				esac
				;;

			ARG_STRESS_NET|ARG_STRESS_NET_REV)
				case $unit in
					K|M)
						;;
					*)
						return 2
						;;
				esac
				;;

			*)
				return 3
				;;

		esac

		num="$(convertParam "$ARG_VALUE" "b" "")"

		if [ $? -ne 0 ]
		then
			return 2
		fi

  else
		case $ARG_VAR in
	  	ARG_MEMORY|ARG_STRESS_MEM)
				unit="m"
				;;
			ARG_STRESS_NET|ARG_STRESS_NET_REV)
				unit="M"
		  	;;
		esac

		if ! [ -z $unit ]
		then
			eval $ARG_VAR="${ARG_VALUE}${unit}"
			num="$(convertParam "${ARG_VALUE}${unit}" "b" "")"
		else
			num="$ARG_VALUE"
		fi

  fi

  if [ ! -z $ARG_MIN -a $num -lt $ARG_MIN ] 2>/dev/null
  then
    return 1
  fi

  if [ ! -z $ARG_MAX -a $num -gt $ARG_MAX ] 2>/dev/null
  then
    return 1
  fi

  return 0
}
###########################################################################################

#################################### checkRequirements ####################################
# Checks if the hardware/software setup fulfills the requirements
# and sets the range for certain arguments, e.g. CPU, RAM
#
# Requires:	HOST1_SSH_MANAGEMENT, HOST2_SSH_MANAGEMENT
#
# Sets:		ARG_CPUS_DEF, ARG_CPUS_MAX, ARG_MEMORY_MAX
checkRequirements() {
	local host1_cpu
	local host2_cpu
	local host1_ram
	local host2_ram
	ARG_CPUS_MAX=""
	ARG_MEMORY_MAX=""

	host1_cpu=$( ssh $HOST1_SSH_MANAGEMENT nproc --all )
	host2_cpu=$( ssh $HOST2_SSH_MANAGEMENT nproc --all )
	host1_ram=$( expr "$( ssh $HOST1_SSH_MANAGEMENT "grep MemFree /proc/meminfo | awk '{print \$2}'" )" \* 1000 )
	host2_ram=$( expr "$( ssh $HOST2_SSH_MANAGEMENT "grep MemFree /proc/meminfo | awk '{print \$2}'" )" \* 1000 )

	if [ $host1_cpu -le $host2_cpu ]; then ARG_CPUS_MAX=$host1_cpu; else ARG_CPUS_MAX=$host2_cpu; fi
	if [ $host1_ram -le $host2_ram ]; then ARG_MEMORY_MAX=$host1_ram; else ARG_MEMORY_MAX=$host2_ram; fi

	ARG_CPUS_DEF=$ARG_CPUS_MAX

	for d in $DIR_APP_ROOT/*
	do
		if [ -d "$d" -a -f "$d/$FILE_APP_CONFIG" ]
		then
			ARG_APPNAME_DEF="$(echo $d | rev | cut -d'/' -f 1 | rev)"
			break
		fi
	done

	for d in $DIR_APP_ROOT/*
	do
    		if [ -d "$d" -a -f "$d/$FILE_APP_CONFIG" ]
		then
        		test123="${test123}$(echo $d | rev | cut -d'/' -f 1 | rev)\n\t\t\t\t\t\t"
    		fi
	done
}
###########################################################################################

###################################### cleanupBetween #####################################
# Performs cleanup operations between each test cycle
#
# Requires:	APP_CLIENT_FILE, ARG_TYPE, APP_CLIENT_BIN, DIR_APP_SRC, DOCKER_CT_CHECKPOINT_DIR,
#		DOCKER_KVM_NAME, FILE_TMP_DUMMY, HOST1_SSH_DATA, HOST1_SSH_MANAGEMENT,
#		HOST2_SSH_MANAGEMENT, KVM_LOG, LOG_TMP_TCPDUMP, SSH_PORT_RESET, TMP_FOLDER,
cleanupBetween() {
	local cmd_cleanup=""
	local echo=""

	echo "********************************** CLEANING UP ****************************"

	case "$ARG_TYPE" in
		docker)
			cmd_cleanup="docker rm -f $DOCKER_KVM_NAME >/dev/null; sudo rm -r $DOCKER_CT_CHECKPOINT_DIR"
			echo="Stopping/Removing container and checkpoint..."
			;;

		kvm)
			cmd_cleanup="\
				virsh destroy $DOCKER_KVM_NAME; \
				virsh undefine $DOCKER_KVM_NAME --remove-all-storage; \
				rm $KVM_VM_VDISK; \
				rm $KVM_LOG; \
				rm $LOG_TMP_TCPDUMP"
			echo="Stopping/Removing VM..."
			;;

	esac

	# Host1
	echo "Host 1: ${echo}"
	ssh $HOST1_SSH_MANAGEMENT "$cmd_cleanup; rm $FILE_TMP_DUMMY" >/dev/null

	# Host2
	echo "Host 2: ${echo}"
	ssh $HOST2_SSH_MANAGEMENT "$cmd_cleanup" >/dev/null

	# Client
	echo "Client: Stopping application"
	ps ax | grep "$APP_CLIENT_BIN $DIR_APP_SRC/$APP_CLIENT_FILE" | grep -v grep | awk '{print $1}' | xargs kill

	# Delete flows Host 2 > Host 1
	# (The SDN controller interprets the SSH/SCP transfer as another migration.)
	echo "Controller: Deleting flows between host2 and host1 ..."
	ssh $HOST2_SSH_MANAGEMENT "scp -P $SSH_PORT_RESET $FILE_TMP_DUMMY $HOST1_SSH_DATA:$TMP_FOLDER"
} 2>/dev/null
###########################################################################################

#################################### cleanupController ####################################
# Stops and removes OpenFlow Controller
#
# Requires:	CONTROLLER_BIN, CONTROLLER_FILE, CONTROLLER_SSH, DIR_TMP
cleanupController(){
  echo "Controller: Stopping and removing Ryu application..."
  ssh $CONTROLLER_SSH "ps ax | grep '$CONTROLLER_BIN $DIR_TMP/$CONTROLLER_FILE' | grep -v grep | awk '{print \$1}' | xargs kill; rm -r $DIR_TMP"
} 2>/dev/null
###########################################################################################

##################################### cleanupStartEnd #####################################
# Performs cleanup operations before & after each test series
#
# Requires:	ARG_TYPE, DIR_TMP, APP_DOCKER_IMAGE, DATA_BRIDGE, DOCKER_KVM_NAME
cleanupStartEnd() {
	local cmd_cleanup=""

	case "$ARG_TYPE" in
		docker)
			cmd_cleanup="docker rm -f $DOCKER_KVM_NAME; docker rmi $APP_DOCKER_IMAGE; sudo rm -r $DIR_TMP; docker network rm $DATA_BRIDGE"
			;;

		kvm)
			cmd_cleanup="\
			virsh destroy $DOCKER_KVM_NAME; \
			virsh undefine $DOCKER_KVM_NAME --remove-all-storage; \
			ps ax | grep 'sudo tcpdump' | grep -v grep | awk '{print \$1}' | xargs sudo kill -2; \
			ps ax | grep 'virsh qemu-monitor-event' | grep -v grep | awk '{print \$1}' | xargs sudo kill -2; \
			sudo rm -r $DIR_TMP"
			;;

	esac

	cmd_cleanup="${cmd_cleanup}; \
	ps ax | grep 'iperf3' | grep -v grep | awk '{print \$1}' | xargs sudo kill -2; \
	ps ax | grep '$DIR_TMP/$FILE_RESMON' | grep -v grep | awk '{print \$1}' | xargs sudo kill"

	# Host1
	echo "Host 1: Cleaning up..."
	ssh $HOST1_SSH_MANAGEMENT "( $cmd_cleanup ) >/dev/null 2>&1"

	# Host2
	echo "Host 2: Cleaning up..."
	ssh $HOST2_SSH_MANAGEMENT "( $cmd_cleanup ) >/dev/null 2>&1"

	# Client
	echo "Client: Stopping and removing applications/monitors..."
	# for proc in "$APP_CLIENT_BIN $DIR_APP_SRC/$APP_CLIENT_FILE" "virt-manager" "iperf3" "ssh $HOST1_SSH_MANAGEMENT" "ssh $HOST2_SSH_MANAGEMENT"
	for proc in "$APP_CLIENT_BIN $DIR_APP_SRC/$APP_CLIENT_FILE"
	do
		ps ax | grep "$proc" | grep -v grep | awk '{print $1}' | xargs kill
	done
	rm -r $DIR_TMP
} 2>/dev/null
###########################################################################################

##################################### controllerSetup #####################################
# Sets up the SDN controller
#
# Requires:	ARG_TYPE, CONTROLLER_BIN, CONTROLLER_LOG, CONTROLLER_FILE, CONTROLLER_SSH,
#		DIR_TMP, DOCKER_KVM_IP, HOST1_MAC_DATA, HOST1_OUT, HOST2_MAC_DATA,
#		HOST2_OUT, OF_COOKIE_MIG, OF_COOKIE_REDIR, OF_TABLE_HW, OF_TABLE_SW,
#		SSH_PORT_RESET, SWITCH_T_INIT, vhost_mac, port_mig
controllerSetup() {
	echo "Controller: Setting up SDN controller ..."
	ssh $CONTROLLER_SSH "mkdir $DIR_TMP"
	scp $DIR_SDNCONTROLLER/$CONTROLLER_FILE $CONTROLLER_SSH:$DIR_TMP >/dev/null
	ssh $CONTROLLER_SSH "$CONTROLLER_FILE_ARGS envsubst <$DIR_TMP/$CONTROLLER_FILE >$DIR_TMP/$CONTROLLER_FILE.tmp;mv $DIR_TMP/$CONTROLLER_FILE.tmp $DIR_TMP/$CONTROLLER_FILE"
	ssh $CONTROLLER_SSH "nohup $CONTROLLER_BIN $DIR_TMP/$CONTROLLER_FILE >$CONTROLLER_LOG 2>&1 &"
	sleep $SWITCH_T_INIT
}
###########################################################################################

############################### convertParam "$1" "$2" "$3" ###############################
# Converts a given value from one unit into another one
#
# Arguments:	1	value to convert
#		2	target unit
#		3	return with/without unit?
#
# Returns:	Converted value with/without unit
#
# Return Code:
#		0	success
#		1	error, value is not a number
#		2	error, value has unknown unit
#		3	error, unknown target unit
#
# Example: 	convertParam "1024m" "g" "y" -> 1g
#		convertParam "1024m" "k" "" -> 1024000k
convertParam() {
	local ARG_VALUE="$1"
	local ARG_UNIT="$2"
	local ARG_WITH_UNIT="$3"

	local num=""
	local unit=""
	local fact=""

	num="$(echo $ARG_VALUE | rev | cut -c 2- | rev)"
	unit="$(echo $ARG_VALUE | rev | cut -c 1)"

	if ! [ $num -le 0 -o $num -gt 0 ] 2>/dev/null
	then
		echo ""
		return 1
	fi

	case $unit in
		b )
			fact=1
			;;
		k )
			fact=1000
			;;
		m )
			fact=1000000
			;;
		g )
			fact=1000000000
			;;
		K )
			fact=8000
			;;
		M )
			fact=8000000
			;;
		*)
			echo ""
			return 2
			;;
	esac

	case $ARG_UNIT in
		b )
			div=1
			;;
		k )
			div=1000
			;;
		m )
			div=1000000
			;;
		g )
			div=1000000000
			;;
		* )
			echo ""
			return 3
			;;
	esac

	if [ -z $ARG_WITH_UNIT ]
	then
		echo "$(expr $num \* $fact / $div)"
	else
		echo "$(expr $num \* $fact / $div)${ARG_UNIT}"
	fi

	return 0
}
###########################################################################################

######################################### log "$1" ########################################
# Calculates and logs measurement values
#
# Arguments:	1	index of current cycle
#
# Returns:	Converted value with/without unit
#
# Requires:	APP_SERVER_FILE, ARG_TYPE, DIR_LOG, DOCKER_KVM_LOG, DOCKER_KVM_NAME,
#		FILE_MEASUREMENTS, HOST1_SSH_MANAGEMENT, HOST2_SSH_MANAGEMENT,
#		KVM_VM_SSH, LOG_TMP_TCPDUMP
log() {
	local cycle="$1"

	case "$ARG_TYPE" in
		docker)
			{
				echo -ne "Host 1 - Container Log\n\n"
				ssh $HOST1_SSH_MANAGEMENT "docker logs $DOCKER_KVM_NAME"
				echo -ne "\n\nHost 2 - Container Log\n\n"
				ssh $HOST2_SSH_MANAGEMENT "docker logs $DOCKER_KVM_NAME"
			} >>"$DIR_LOG/${cycle}_${DOCKER_KVM_NAME}.log"
			;;

		kvm)
			scp $KVM_VM_SSH:$DOCKER_KVM_LOG "$DIR_LOG/${cycle}_${DOCKER_KVM_NAME}.log" > /dev/null

			for var in log_tcpdump_1 log_tcpdump_2 log_kvm_1 log_kvm_2 t_mig_setup_1 t_mig_setup_2 t_mig_active_1 t_mig_active_2 t_mig_complete_1 t_mig_complete_2 t_mig_stop t_mig_vserportchange t_mig_resume t_tcp_start_1 t_tcp_start_2 t_tcp_stop_1 t_tcp_stop_2 T_precpy T_stop T_cpy T_start T_m M mem_app
			do
				eval "local $var"
			done

			log_tcpdump_1="$DIR_LOG/${cycle}_tcpdump_host1.log"
			log_tcpdump_2="$DIR_LOG/${cycle}_tcpdump_host2.log"
			log_kvm_1="$DIR_LOG/${cycle}_kvm_host1.log"
			log_kvm_2="$DIR_LOG/${cycle}_kvm_host2.log"

			{
				scp $HOST1_SSH_MANAGEMENT:$LOG_TMP_TCPDUMP "$log_tcpdump_1"
				scp $HOST2_SSH_MANAGEMENT:$LOG_TMP_TCPDUMP "$log_tcpdump_2"
				scp $HOST1_SSH_MANAGEMENT:$KVM_LOG "$log_kvm_1"
				scp $HOST2_SSH_MANAGEMENT:$KVM_LOG "$log_kvm_2"

			} >/dev/null

			t_mig_setup_1=$( grep "event MIGRATION" "$log_kvm_1" | grep "\"status\":\"setup\"" | head -1 | cut -d" " -f4 )
			t_mig_active_1=$( grep "event MIGRATION" "$log_kvm_1" | grep "\"status\":\"active\"" | head -1 | cut -d" " -f4 )
			t_mig_stop=$( grep "event STOP" "$log_kvm_1" | head -1 | cut -d" " -f4 )
			t_mig_complete_1=$( grep "event MIGRATION" "$log_kvm_1" | grep "\"status\":\"completed\"" | head -1 | cut -d" " -f4 )

			t_mig_setup_2=$( grep "event MIGRATION" "$log_kvm_2" | grep "\"status\":\"setup\"" | head -1 | cut -d" " -f4 )
			t_mig_active_2=$( grep "event MIGRATION" "$log_kvm_2" | grep "\"status\":\"active\"" | head -1 | cut -d" " -f4 )
			t_mig_vserportchange=$( grep "event VSERPORT_CHANGE" "$log_kvm_2" | grep "\"open\":true" | head -1 | cut -d" " -f4 )
			t_mig_complete_2=$( grep "event MIGRATION" "$log_kvm_2" | grep "\"status\":\"completed\"" | head -1 | cut -d" " -f4 )
			t_mig_resume=$( grep "event RESUME" "$log_kvm_2" | head -1 | cut -d" " -f4 )

			t_tcp_start_1=$( head -3 "$log_tcpdump_1" | tail -1 | cut -d" " -f1 )
			t_tcp_stop_1=$( tail -5 "$log_tcpdump_1" | head -1 | cut -d" " -f1 )

			t_tcp_start_2=$( head -3 "$log_tcpdump_2" | tail -1 | cut -d" " -f1 )
			t_tcp_stop_2=$( tail -5 "$log_tcpdump_2" | head -1 | cut -d" " -f1 )

			T_precpy=$( echo "scale=6 ; $t_mig_stop-$t_mig_active_1" | bc )
			T_precpy_log=$(echo "scale=0; $T_precpy * 1000" | bc)

			T_cpy=$( echo "scale=6 ; $t_mig_complete_1-$t_mig_stop" | bc )
			T_cpy_log=$(echo "scale=0; $T_cpy * 1000" | bc)

			# T_start=$( echo "scale=6 ; $t_mig_resume-$t_mig_complete_2" | bc )
			T_start=$( echo "scale=6 ; $t_mig_resume-$t_mig_vserportchange" | bc )
			T_start_log=$(echo "scale=0; $T_start * 1000" | bc)

			T_m=$( echo "scale=6 ; $T_precpy + $T_cpy + $T_start" | bc )
			T_m_log=$(echo "scale=0; $T_m * 1000" | bc)

			M=$(cat $log_tcpdump_1  | tail -n +3 | head -n -4 | awk '{ sum += $7 } END { print sum }')
			mem_app="$(ssh $KVM_VM_SSH "cat /proc/\$(ps -o pid,args | grep -E '^\s[0-9]+\s($APP_CLIENT_CMD $APP_SERVER_FILE)' | grep -v grep | cut -d' ' -f2)/statm | cut -d' ' -f2")"
			mem_app="$(expr $mem_app \* 4 \* 1024)"
			T_d=$(measureDowntime "$cycle" "evaluate")
			# T_d="$(cat "$DIR_LOG/${cycle}_${DOCKER_KVM_NAME}.log" | grep "Delay:" | tail -1 | cut -d" " -f2)"

			# column order: T_m;T_precpy;T_stop;T_copy;T_start;M;mem_app;T_d
			echo -ne "$T_m_log;$T_precpy_log;$T_cpy_log;$T_start_log;$M;$mem_app;$T_d\n" >> "$FILE_MEASUREMENTS"
			;;
	esac
}
###########################################################################################

################################ measureDowntime "$1" "$2" ################################
# Measures container/VM downtime
#
# Arguments:	1	index of current cycle
#		2	phase
#			Value		Description
#			start		start measurement
#			stop		stop measurement
#			evaluate	calculate downtime from logfile
#
# Returns:	downtime in ms
#
# Requires:	ARG_DOWNTIME, CLIENT_IF_DATA, CLIENT_IP_DATA, CLIENT_PING_PORT,
#		DIR_LOG, DOCKER_KVM_IP
measureDowntime() {
	local cycle="$1"
	local cmd="$2"
	local log_tcpdump="$DIR_LOG/${cycle}_tcpdump_client.log"

	case "$cmd" in
		start)
			sudo tcpdump -tt --time-stamp-precision=micro -i $CLIENT_IF_DATA -q -l --immediate-mode "src ${DOCKER_KVM_IP} and dst ${CLIENT_IP_DATA} and udp port ${CLIENT_PING_PORT}" >> $log_tcpdump 2>&1 &
			;;
		stop)
			ps ax | grep 'tcpdump' | grep -v grep | awk '{print $1}' | xargs sudo kill -2
			;;
		evaluate)
			tail -n +3 "$log_tcpdump" | head -n -4 | awk -v ARG_DOWNTIME="$ARG_DOWNTIME" '
				BEGIN {
					T_d=""
				}

				NR == 1 {
					t_before=$1
					next
				}

				{
					t_delta = ($1-t_before) * 1000

					if (t_delta > ARG_DOWNTIME) {
						t_delta = t_delta

						if (T_d != "") {
							T_d = T_d " ++ " t_delta
						} else {
							T_d = t_delta
						}
					}

					t_before=$1
				}

				END {
					print T_d
				}'
				;;
	esac
}
###########################################################################################

################################# measureNetworkCapacity ##################################
# Measures and logs the network's (idle) capacity
#
# Requires:	HOST1_SSH_MANAGEMENT, HOST2_SSH_MANAGEMENT, HOST2_IP_DATA, LOG_IPERF_CAP
#
# Returns:	CSV lines with measurements (h1->2 and h2->h1)
measureNetworkCapacity() {
	local c_h1_h2=""
	local c_h2_h1=""

	ssh $HOST2_SSH_MANAGEMENT "sudo iperf3 -s -D -B $HOST2_IP_DATA"
	ssh $HOST1_SSH_MANAGEMENT "iperf3 -c $HOST2_IP_DATA -i 1 -t 5 -f m" >$LOG_IPERF_CAP 2>&1
	ssh $HOST1_SSH_MANAGEMENT "iperf3 -c $HOST2_IP_DATA -i 1 -t 5 -f m -R" >>$LOG_IPERF_CAP 2>&1

	c_h1_h2=$(cat $LOG_IPERF_CAP | grep -E "^(- )+-$" -A3 | grep "receiver" | head -1 | awk -F '[ ]+' '{print $7}')
	c_h2_h1=$(cat $LOG_IPERF_CAP | grep -E "^(- )+-$" -A3 | grep "receiver" | tail -1 | awk -F '[ ]+' '{print $7}')

	ssh $HOST2_SSH_MANAGEMENT "ps ax | grep 'iperf3' | grep -v grep | awk '{print \$1}' | xargs sudo kill -2"
	echo "C_+;C_-\nMbit/s;Mbit/s\n$c_h1_h2;$c_h2_h1"
}
###########################################################################################

####################################### migrate "$1" ######################################
# Migrate the container/VM and measure migration times and downtime
#
# Arguments:	1	index of current cycle
#
# Requires:	ARG_TYPE, DATA_BRIDGE, DIR_TMP, DOCKER_CT_CHECKPOINT_NAME,
#		DOCKER_KVM_NAME, DOCKER_PORT_MIG, FILE_MEASUREMENTS, HOST1_SSH_MANAGEMENT,
#		HOST2_SSH_DATA, HOST2_SSH_MANAGEMENT, KVM_LOG, KVM_PORT_MIG,
#		KVM_VM_VDISK, LOG_TMP_TCPDUMP, cmd_docker_update, cmd_taskset
migrate() {
	local cycle="$1"

	case "$ARG_TYPE" in
		docker)

			measureDowntime "$cycle" "start"

			# Host1
			echo "Host 1: Creating checkpoint..."
			timestamp1=$(timestamp)
			ssh $HOST1_SSH_MANAGEMENT "${cmd_taskset}docker checkpoint create --checkpoint-dir=$DIR_TMP $DOCKER_KVM_NAME $DOCKER_CT_CHECKPOINT_NAME >/dev/null"
			timestamp2=$(timestamp)

			# Host1 -> Host2
			echo "Host 1: Transferring checkpoint to Host 2"
			ssh $HOST1_SSH_MANAGEMENT "${cmd_taskset}scp -P $DOCKER_PORT_MIG -r $DOCKER_CT_CHECKPOINT_DIR $HOST2_SSH_DATA:$DIR_TMP >/dev/null 2>&1"
			timestamp3=$(timestamp)

			# Host2
			echo "Host 2: Restoring container from checkpoint..."
			ssh $HOST2_SSH_MANAGEMENT "${cmd_taskset}docker start --checkpoint-dir=$DIR_TMP --checkpoint=$DOCKER_CT_CHECKPOINT_NAME $DOCKER_KVM_NAME >/dev/null"
			timestamp4=$(timestamp)
			echo "Host 2: Container restored..."
			eval $cmd_docker_update # Dedicating CPUs

			measureDowntime "$cycle" "stop"

			# Benchmark data
			T_m="$(expr $timestamp4 - $timestamp1)"
			T_chk="$(expr $timestamp2 - $timestamp1)"
			T_cpy="$(expr $timestamp3 - $timestamp2)"
			T_start="$(expr $timestamp4 - $timestamp3)"
			M="$(ssh $HOST1_SSH_MANAGEMENT "du -bs $DOCKER_CT_CHECKPOINT_DIR | cut -f1")"
			T_d="$(measureDowntime "$cycle" "evaluate")"
			echo -ne "$T_m;$T_chk;$T_cpy;$T_start;$M;$T_d\n" >> "$FILE_MEASUREMENTS"
			;;

		kvm)
			# Host 1
			ssh $HOST1_SSH_MANAGEMENT /bin/sh <<-EOF
				# Copy image
				# rsync -avz --sparse --progress $KVM_VM_VDISK $HOST2_SSH_DATA:$DIR_TMP
				echo "Host 1: Copying VM disk to Host 2 ..."
				sudo chmod 777 $KVM_VM_VDISK
				scp $KVM_VM_VDISK $HOST2_SSH_DATA:$DIR_TMP

				# Start Monitoring
				echo "Host 1: Starting tcpdump & QEMU monitor ..."
				sudo tcpdump -tt --time-stamp-precision=micro -l --immediate-mode -i $DATA_BRIDGE -q tcp dst port $KVM_PORT_MIG >> $LOG_TMP_TCPDUMP 2>&1 &
				virsh qemu-monitor-event --loop >> $KVM_LOG &
			EOF

			# Host 2
			ssh $HOST2_SSH_MANAGEMENT /bin/sh <<-EOF
				# Start Monitoring
				echo "Host 2: Starting tcpdump & QEMU monitor ..."
				sudo tcpdump -tt --time-stamp-precision=micro -l --immediate-mode -i $DATA_BRIDGE -q tcp dst port $KVM_PORT_MIG >> $LOG_TMP_TCPDUMP 2>&1 &
				virsh qemu-monitor-event --loop >> $KVM_LOG &
			EOF

			measureDowntime "$cycle" "start"

			# Migration Host 1 -> Host 2
			ssh $HOST1_SSH_MANAGEMENT /bin/sh <<-EOF
				echo "Host 1: Starting migration to Host 2..."
				virsh migrate $DOCKER_KVM_NAME qemu+ssh://$HOST2_SSH_DATA/system --live --undefinesource > /dev/null
				echo "VM migrated. Waiting for being reachable again ..."
			EOF
			waitUntilAlive "$DOCKER_KVM_IP" "22"
			echo "VM is now reachable again."

			measureDowntime "$cycle" "stop"

			# Host1, Host2: Quitting tcpdump and virsh qemu-monitor
			for ssh in "$HOST1_SSH_MANAGEMENT" "$HOST2_SSH_MANAGEMENT"
			do
				ssh $ssh /bin/sh <<-EOF
					ps ax | grep 'sudo tcpdump' | grep -v grep | awk '{print \$1}' | xargs sudo kill -2
					ps ax | grep 'virsh qemu-monitor-event' | grep -v grep | awk '{print \$1}' | xargs sudo kill -2
				EOF
			done
			;;
	esac
}
###########################################################################################

########################## prepareHost "$1" "$2" "$3" "$4" "$5" ###########################
# Configure the host's network interfaces and bootloader (CPU core isolation)
#
# Arguments:	1	SSH URI, e.g. ubuntu@10.0.0.101
#		2	Name of DATA interface
#		3	IP of DATA interface
#		4	Name of MANAGEMENT interface
#		5	IP of MANAGEMENT interface
#
# Requires:	ARG_CPUS, ARG_DEDICATED, ARG_TYPE, DATA_BRIDGE, DATA_MASK,
#		DOCKER_KVM_NAME, HOST2_SSH_MANAGEMENT, MANAGEMENT_GATEWAY
prepareHost() {
	local ssh="$1"
	local data_interface="$2"
	local data_ip="$3"
	local management_interface="$4"
	local management_ip="$5"

	local isolcpus=""

	# Network Configuration (1)
	ssh "$ssh" "sudo sh -c 'cat >/etc/network/interfaces'" <<-EOF
		# Loopback
		auto lo
		iface lo inet loopback

		# Management Interface
		auto $management_interface
		iface $management_interface inet static
		     address $management_ip
		     netmask $MANAGEMENT_MASK
		     gateway $MANAGEMENT_GATEWAY
		     dns-nameservers $MANAGEMENT_GATEWAY

	EOF

	# Network Configuration (2)
	case "$ARG_TYPE" in
		docker)
			ssh "$ssh" "sudo sh -c 'cat >>/etc/network/interfaces'" <<-EOF
				# Data Interface
				auto $data_interface
				iface $data_interface inet static
				     address $data_ip
				     netmask $DATA_MASK

			EOF
			;;
		kvm)
			ssh "$ssh" "sudo sh -c 'cat >>/etc/network/interfaces'" <<-EOF
				# Data Interface
				auto $DATA_BRIDGE
				iface $DATA_BRIDGE inet static
				     address $data_ip
				     netmask $DATA_MASK
				     bridge_ports $data_interface
				     bridge_stp off
				     bridge_fd 0

			EOF
			;;
	esac

	# Dedicating CPUs
	if [ "$ARG_DEDICATED" = "1" ]
	then
		local core_last=$(expr $ARG_CPUS - 1)
		isolcpus="1"
		cmd_taskset="taskset -c 1 "
		cmd_chrt="sudo chrt -r 10 "
		cmd_docker_update="ssh $HOST2_SSH_MANAGEMENT \"${cmd_taskset}docker update --cpuset-cpus=2-$core_last $DOCKER_KVM_NAME >/dev/null\""

		i=2
		while [ $i -le $core_last ]
		do
			isolcpus="$isolcpus,$i"
			i=$(expr $i + 1)
		done

		isolcpus=" isolcpus=$isolcpus"
	fi

	# Editing bootloader configuration
	ssh "$ssh" "sudo sed -i 's/^\(GRUB_CMDLINE_LINUX_DEFAULT\s*=\s*\)\".*\"\s*\$/\1\"${HOST12_GRUB}${isolcpus}\"/' /etc/default/grub && sudo update-grub 2>/dev/null"

	# Reboot
	ssh "$ssh" "sudo reboot"

} >/dev/null
###########################################################################################

####################################### printParams #######################################
# Prints chosen parameters onto the terminal
#
# Requires:	ARG_CPUS, ARG_DEDICATED, ARG_MEMORY, ARG_RUN, ARG_TYPE
#		ARG_STRESS_CPU, ARG_STRESS_MEM, ARG_STRESS_NET,ARG_STRESS_NET_REV
printParams() {
	cat <<EOF

Virtualisation Parameters

Test Application a:		$ARG_APPNAME
Virtualisation Type t:		$ARG_TYPE
Number of cycles r:		$ARG_RUN
Virtual CPU cores c:		$ARG_CPUS core(s)
-- dedicated mode d:		$ARG_DEDICATED
Virtual Memory m:		$ARG_MEMORY

Key Performance Indicators (KPIs)

CPU Usage R_CPU :		$ARG_STRESS_CPU
RAM Usage R_RAM :		$ARG_STRESS_MEM
Network Load (h1->h2) D+ :	$ARG_STRESS_NET
Network Load (h2->h1) D- :	$ARG_STRESS_NET_REV

*********************************************************************************************

EOF
}
###########################################################################################

####################################### printResult #######################################
# Prints closing message onto the terminal
#
# Requires:	DIR_LOG, FILE_MEASUREMENTS
printResult() {
	cat <<EOF

*********************************************************************************************
*********************************** MEASUREMENTS FINISHED ***********************************
*********************************************************************************************
EOF
	printParams
	cat <<EOF
Results

EOF

	cat "$FILE_MEASUREMENTS"
	cat <<EOF

*********************************************************************************************
All log files can be found under: $DIR_LOG
*********************************************************************************************
EOF
}
###########################################################################################

######################################## printUsage #######################################
# Prints user information to the terminal
#
# Requires:	ARG_CPUS_DEF, ARG_CPUS_MAX, ARG_MEMORY_MIN, ARG_MEMORY_MAX
#		ARG_RUN_DEF, ARG_RUN_MIN, ARG_RUN_MAX, ARG_TYPE_DEF
#		ARG_STRESS_CPU_DEF, ARG_STRESS_CPU_MIN, ARG_STRESS_CPU_MAX
#		ARG_STRESS_MEM_DEF, ARG_STRESS_NET_DEF, ARG_STRESS_NET_REV_DEF
printUsage() {
	local scriptname="$(basename $0)"
	cat <<EOF

Usage: $scriptname [OPTION]... [MEASUREMENT_DIR]

Performs a migration benchmark and saves meausrements into [MEASUREMENT_DIR].

Options:
	-h, --help				show this help message and exit

	-a, --app APPNAME			test application to use
						(default: $ARG_APPNAME_DEF)

						APPNAME
						------------------------------------
						$(echo -ne "$test123")

	-c, --cpus CPUS				number of physical CPUs available for this test
						$ARG_CPUS_MIN...$ARG_CPUS_MAX (default: $ARG_CPUS_DEF)

	-d, --dedicated				dedicate the CPU cores

						TYPE	Core		Assigned to
						---------------------------------------------------
						docker	0 		host OS
							1 		Docker daemon & migration
							2...CPUS 	container
						kvm	0		host OS & KVM daemon & migr.
							1...CPUS 	virtual machine

     	-m, --memory MEM			amount of physical RAM assigned to the container/VM
		 				in Bytes (b), Kilo- (k), Mega- (m) or Gigabytes (g)
						$(expr $ARG_MEMORY_MIN / 1000000)m...$(expr $ARG_MEMORY_MAX / 1000000)m [b|k|m|g] (default: $ARG_MEMORY_DEF)

	-r, --run RUN				number of test cycles
		 				$ARG_RUN_MIN...$ARG_RUN_MAX (default: $ARG_RUN_DEF)

     	-t, --type TYPE				set virtualisation type
		 				[docker|kvm] (default: $ARG_TYPE_DEF)

	-w, --downtime TIME			set treshold value (in ms) for downtime measurement
						$ARG_DOWNTIME_MIN...$ARG_DOWNTIME_MAX (default: $ARG_DOWNTIME_DEF)

     	-C, --stress-cpu STRESSCPU		enable CPU stress (in percentage/core)
		 				$ARG_STRESS_CPU_MIN...$ARG_STRESS_CPU_MAX (default: $ARG_STRESS_CPU_DEF)

     	-M, --stress-mem STRESSMEM		enable RAM stress
		 				in Bytes (b), Kilo- (k), Mega- (m) or Gigabytes (g)
						0...$(expr $ARG_MEMORY_MAX / 1000000)m [b|k|m|g] (default: $ARG_STRESS_MEM_DEF)

     	-N, --stress-net STRESSNET		enable network stress (same direction as migration)
		 				in KBit/s (K) or Mbit/s (M)
						... [K|M] (default: $ARG_STRESS_NET_DEF)

     	-O, --stress-net-rev STRESSNETREV  	enable network stress (opposite direction)
		 				in KBit/s (K) or Mbit/s (M)
						... [K|M] (default: $ARG_STRESS_NET_REV_DEF)

Examples:
	$scriptname -d -m 256m -r 50 -t kvm -C 90
	$scriptname -m 128m -r 50 -t docker -N 900M "/tmp/measurements"

EOF
}
###########################################################################################

###################################### printWelcome #######################################
# Prints initial message onto the terminal
#
printWelcome() {
	cat <<EOF

*********************************************************************************************
***************************** ComNets Live Migration Benchmark ******************************
*********************************************************************************************

Author:		Florian Kemser
		florian.kemser@mailbox.tu-dresden.de

Institution:	Technische Universität Dresden
		Faculty of Electrical and Computer Engineering
		Institute of Communication Technology
		Deutsche Telekom Chair of Communication Networks

Date:		Aug 2019 - Feb 2020

*********************************************************************************************
EOF
	printParams
}
###########################################################################################

########################################### end ###########################################
# Performs last steps before finishing the test series, e.g. copying log files, cleaning up
#
# Requires:	CONTROLLER_LOG, CONTROLLER_SSH, DIR_LOG, DIR_NAME, DIR_TMP,
#		FILE_RESMON, HOST1_SSH_MANAGEMENT, HOST2_SSH_MANAGEMENT
end() {
	# Copying protocol files
	{

		# Host1, Host2: Quitting iperf3 and resource monitor
		for ssh in "$HOST1_SSH_MANAGEMENT" "$HOST2_SSH_MANAGEMENT"
		do
			ssh $ssh /bin/sh <<-EOF
				ps ax | grep 'iperf3' | grep -v grep | awk '{print \$1}' | xargs sudo kill -2
				ps ax | grep '$DIR_TMP/$FILE_RESMON' | grep -v grep | awk '{print \$1}' | xargs sudo kill
			EOF
		done

		scp $CONTROLLER_SSH:$CONTROLLER_LOG $DIR_LOG
		scp $HOST1_SSH_MANAGEMENT:$LOG_TMP_IPERF $LOG_IPERF_STRESS_H1H2
		scp $HOST2_SSH_MANAGEMENT:$LOG_TMP_IPERF $LOG_IPERF_STRESS_H2H1
		scp $HOST1_SSH_MANAGEMENT:$FILE_TMP_RESMON $FILE_RESMON_H1
		scp $HOST2_SSH_MANAGEMENT:$FILE_TMP_RESMON $FILE_RESMON_H2
	} >/dev/null 2>&1

	echo "********************************** CLEANING UP **********************************************"
	cleanupStartEnd
	cleanupController

	printResult
}
###########################################################################################

###################################### runCycle "$1" ######################################
# Migrates the container/VM and measures migration times and downtime
#
# Requires:	ARG_TYPE, DIR_APP_SRC_FOLDERNAME, DIR_TMP, DOCKER_KVM_IP, DOCKER_KVM_NAME,
#		HOST1_SSH_MANAGEMENT, HOST2_SSH_MANAGEMENT, APP_KVM_VM_IMAGE, KVM_VM_VDISK,
#		KVM_VM_SSH, cmd_chrt, vhost_setup_args
runCycle() {
	local cycle="$1"

	echo "********************************** CYCLE no. ${cycle} ***************************************"

	case "$ARG_TYPE" in
		docker)
			local docker_cid=""

			# Host 2
			echo "Host 2: Creating container ..."
			ssh $HOST2_SSH_MANAGEMENT "docker create $vhost_setup_args >/dev/null"

			# Host 1
			echo "Host 1: Creating container and starting ..."
			ssh $HOST1_SSH_MANAGEMENT "${cmd_chrt}docker run --detach $vhost_setup_args >/dev/null"

			# Container: create symlink for CPU accounting controller
			docker_cid="$(ssh $HOST1_SSH_MANAGEMENT "docker ps --no-trunc | grep $DOCKER_KVM_NAME | cut -d ' ' -f1")"
			ssh $HOST2_SSH_MANAGEMENT "sudo mkdir -p /sys/fs/cgroup/cpu/docker/$docker_cid && ln -sfn /sys/fs/cgroup/cpu/docker/$docker_cid/cpuacct.usage_percpu $DIR_TMP/cpuacct.usage_percpu"
			;;

		kvm)
			# Host 1
			echo "Host 1: Creating and starting VM ..."
			ssh $HOST1_SSH_MANAGEMENT /bin/sh >/dev/null <<-EOF
				cp $DIR_TMP/$APP_KVM_VM_IMAGE $KVM_VM_VDISK
				virt-install $vhost_setup_args
				$cmd_kvm_cpupin
				EOF

			echo "VM: Booting..."
			waitUntilAlive "$DOCKER_KVM_IP" "22"

			# VM
			ssh $KVM_VM_SSH /bin/sh <<-EOF
				# Create TMP directory
				mkdir $DIR_TMP
				chmod 777 $DIR_TMP

				# Create RAMDisk
				mount -t tmpfs -o size=200M none $DIR_TMP
			EOF

			echo "VM: Copying and starting application ..."
			scp -r "$DIR_APP_SRC" "$KVM_VM_SSH:$DIR_TMP" >/dev/null
			ssh "$KVM_VM_SSH" "\
				cd $DIR_TMP/$DIR_APP_SRC_FOLDERNAME; \
				export $APP_ENV_KVM; \
				chmod +x init.sh; \
				./init.sh >$DOCKER_KVM_LOG 2>&1 &"
			;;

	esac

	# Client
	echo "Client: Starting application..."
	$APP_CLIENT_BIN $DIR_APP_SRC/$APP_CLIENT_FILE $APP_CLIENT_FILE_ARGS >"$DIR_LOG/${cycle}_client.log" 2>&1 &

	# Let it run for some time
	sleep $DOCKER_KVM_T_UP_BEFORE

	# VM: clear buffer/cache
	# ssh $KVM_VM_SSH "sync && echo 3 >/proc/sys/vm/drop_caches"

	# Migration
	migrate "$cycle"

	# Let it run for some time
	sleep $DOCKER_KVM_T_UP_AFTER

	# Log results
	log "$cycle"

	# Cleanup
	cleanupBetween
}
###########################################################################################

###################################### updateGlobals ######################################
# Sets constants and variables depending on the passed arguments
#
# Requires:	APP_ENV_DOCKER APP_ENV_KVM CONTROLLER_FILE_ARGS DIR_NAME DIR_LOG
#		FILE_MEASUREMENTS FILE_MEASUREMENTS_HEADER FILE_RESMON_H1 FILE_RESMON_H2
#		KVM_VM_SSH LOG_IPERF_CAP LOG_IPERF_STRESS_H1H2 LOG_IPERF_STRESS_H2H1
#
# Sets:		csv_header port_mig vhost_mac ARG_MEMORY
#		APP_CLIENT_BIN APP_CLIENT_CMD APP_ENV_DOCKER APP_ENV_KVM CONTROLLER_FILE_ARGS
#		DIR_LOG DIR_NAME FILE_MEASUREMENTS FILE_MEASUREMENTS_HEADER FILE_RESMON_H1
#		FILE_RESMON_H2 KVM_VM_SSH LOG_IPERF_CAP LOG_IPERF_CAP
#		LOG_IPERF_STRESS_H1H2 LOG_IPERF_STRESS_H2H1
updateGlobals() {
	case "$ARG_TYPE" in
		docker)
			csv_header="$DOCKER_CSV_HEADER"
			port_mig="$DOCKER_PORT_MIG"
			vhost_mac="$DOCKER_CT_MAC"
			;;
		kvm)
			csv_header="$KVM_CSV_HEADER"
			port_mig="$KVM_PORT_MIG"
			vhost_mac="$KVM_VM_MAC"
			ARG_MEMORY="$(echo $ARG_MEMORY | rev | cut -c 2- | rev)"
			;;
	esac

	# Update application config file INDEPENT constants
	for const in DIR_APP DIR_APP_KVM DIR_APP_SRC DIR_APP_SRC_FOLDERNAME CONTROLLER_FILE_ARGS DIR_NAME DIR_LOG FILE_MEASUREMENTS FILE_MEASUREMENTS_HEADER FILE_RESMON_H1 FILE_RESMON_H2 LOG_IPERF_CAP LOG_IPERF_STRESS_H1H2 LOG_IPERF_STRESS_H2H1
	do
		eval eval $const="\"\$$const\""
	done

	# Import application configuration file
	source "$DIR_APP/$FILE_APP_CONFIG"
	APP_CLIENT_CMD="$(echo $APP_CLIENT_BIN | awk 'BEGIN { FS = "/" } ; {print $NF}' | sed 's/ *$//g')"

	# Update application config file DEPENDENT constants
	for const in APP_ENV_DOCKER APP_ENV_KVM KVM_VM_SSH
	do
		eval eval $const="\"\$$const\""
	done
}
###########################################################################################

####################################### setDefaults #######################################
# Initialises the constants / variables with its default values
#
# Requires:	ARG_APPNAME_DEF ARG_CPUS_DEF ARG_DEDICATED_DEF ARG_DOWNTIME_DEF ARG_MEMORY_DEF ARG_RUN_DEF ARG_TYPE_DEF
#		ARG_STRESS_CPU_DEF ARG_STRESS_MEM_DEF ARG_STRESS_NET_DEF ARG_STRESS_NET_REV_DEF
#
# Sets:		ARG_APPNAME ARG_CPUS ARG_DEDICATED ARG_DOWNTIME ARG_MEMORY ARG_RUN ARG_TYPE
#		ARG_STRESS_CPU ARG_STRESS_MEM ARG_STRESS_NET ARG_STRESS_NET_REV
#		csv_header cmd_chrt cmd_docker_update cmd_kvm_cpupin cmd_taskset
#		port_mig vhost_mac vhost_setup_args
setDefaults() {
	# Constants
	for var in ARG_APPNAME ARG_CPUS ARG_DEDICATED ARG_DOWNTIME ARG_MEMORY ARG_RUN ARG_TYPE ARG_STRESS_CPU ARG_STRESS_MEM ARG_STRESS_NET ARG_STRESS_NET_REV
	do
		eval $var="\$${var}_DEF"
	done

	# Global variables
	for var in csv_header cmd_chrt cmd_docker_update cmd_kvm_cpupin cmd_taskset port_mig vhost_mac vhost_setup_args
	do
		eval $var=""
	done
}
###########################################################################################

########################################## setup ##########################################
# Sets up the test environment: configuring hosts, SDN controller, syncing time, ...
#
# Requires:	ARG_CPUS, ARG_DEDICATED, ARG_MEMORY, ARG_TYPE
#		APP_ENV_DOCKER, APP_ENV_KVM, CLIENT_IP_MANAGEMENT, CONTROLLER_SSH,
#		DIR_LOG, DIR_TMP, APP_DOCKER_IMAGE, CONTROLLER_FILE,
#		FILE_MEASUREMENTS_HEADER, FILE_TMP_DUMMY, FILE_TMP_RESMON, KVM_VM_VDISK,
#		HOST1_IF_DATA, HOST1_IP_DATA, HOST1_IP_MANAGEMENT, HOST1_SSH_MANAGEMENT,
#		HOST1_MAC_DATA, HOST1_OUT, HOST2_IF_MANAGEMENT, HOST2_IP_MANAGEMENT,
#		HOST2_SSH_MANAGEMENT, MANAGEMENT_GATEWAY, DATA_BRIDGE, DOCKER_KVM_NAME
#		vhost_mac
#
# Sets:		cmd_kvm_cpupin, vhost_setup_args
setup() {
	local cmd_timesync="sudo ntpdate $CLIENT_IP_MANAGEMENT"
	local csv_network_capacity=""

	echo "********************************** SETUP ****************************************************"

	cleanupStartEnd

	# Client: Creating Log Folder & Resetting Webcam
	echo "Client: Creating log folder and resetting webcam ..."
	mkdir -p "$DIR_LOG"
	if [ -f "$DIR_APP_SRC/$FILE_APP_SETUP_CLIENT" ]; then source "$DIR_APP_SRC/$FILE_APP_SETUP_CLIENT"; fi

	# Host 1/2: Setup
	echo "Host 1/2: Configuring and rebooting ..."
	prepareHost "$HOST1_SSH_MANAGEMENT" "$HOST1_IF_DATA" "$HOST1_IP_DATA" "$HOST1_IF_MANAGEMENT" "$HOST1_IP_MANAGEMENT" "$MANAGEMENT_GATEWAY"
	prepareHost "$HOST2_SSH_MANAGEMENT" "$HOST2_IF_DATA" "$HOST2_IP_DATA" "$HOST2_IF_MANAGEMENT" "$HOST2_IP_MANAGEMENT" "$MANAGEMENT_GATEWAY"

	# Initialising SDN controller
	cleanupController
	controllerSetup

	# Waiting for hosts to be ready
	echo "Host 1/2: Waiting to be up ..."
	waitUntilAlive "$HOST1_IP_MANAGEMENT" "22"
	waitUntilAlive "$HOST2_IP_MANAGEMENT" "22"
	sleep 5

	# Measuring Network Capacity
	echo "Measuring current network capacity ..."
	csv_network_capacity="$(measureNetworkCapacity)"

	case "$ARG_TYPE" in
		docker)
			local cmd_setup="mkdir $DIR_TMP; docker pull $APP_DOCKER_IMAGE"
			local cpu_args=""

			# Dedicated CPU mode settings
			if [ "$ARG_DEDICATED" = "1" ]
			then
				cmd_setup="$cmd_setup; \
					local pid_dockerd=\"\$(ps ax | grep dockerd | grep -v grep | awk '{print \$1}')\"; \
					local pid_containerd=\"\$(ps ax | grep containerd | grep -v grep | awk '{print \$1}')\"; \
					sudo taskset -ac -p 1 \$pid_dockerd; \
					sudo taskset -ac -p 1 \$pid_containerd; \
					sudo chrt -ar -p 10 \$pid_dockerd; \
					sudo chrt -ar -p 10 \$pid_containerd"
				cpu_args="--cpuset-cpus=\"2-$(expr $ARG_CPUS - 1)\""
			fi

			# Container setup arguments
			vhost_setup_args="\
				--name $DOCKER_KVM_NAME \
				$cpu_args \
				--memory=$ARG_MEMORY --memory-swap=$ARG_MEMORY \
				--net=$DATA_BRIDGE --mac-address=$vhost_mac --ip=$DOCKER_KVM_IP \
				$APP_ENV_DOCKER \
				$APP_DOCKER_IMAGE"

			# Setting up network and loading image
			echo "Host 1/2: Setting up Docker network and image ..."
			ssh $HOST1_SSH_MANAGEMENT "($cmd_setup; docker network create -d macvlan --subnet=$DATA_NET -o parent=$HOST1_IF_DATA $DATA_BRIDGE) >/dev/null"
			ssh $HOST2_SSH_MANAGEMENT "($cmd_setup; docker network create -d macvlan --subnet=$DATA_NET -o parent=$HOST2_IF_DATA $DATA_BRIDGE; touch $FILE_TMP_DUMMY) >/dev/null"

			;;

		kvm)
			local cpu_args=""

			# Dedicated CPU mode settings
			if [ "$ARG_DEDICATED" = "1" ]
			then
				cpu_args="--cpuset=\"1-$(expr $ARG_CPUS - 1)\" --vcpus=$(expr $ARG_CPUS - 1)"

				local vcpu=0;
				local pcpu=1;
				while [ $pcpu -lt $(expr $ARG_CPUS - 1) ]
				do
					cmd_kvm_cpupin="${cmd_kvm_cpupin}virsh vcpupin $DOCKER_KVM_NAME $vcpu $pcpu;"
					vcpu=$(expr $vcpu + 1)
					pcpu=$(expr $pcpu + 1)
				done
				cmd_kvm_cpupin="${cmd_kvm_cpupin}virsh vcpupin $DOCKER_KVM_NAME $vcpu $pcpu"
			fi

			# VM setup arguments
			vhost_setup_args="\
			--connect qemu:///system \
			--name $DOCKER_KVM_NAME \
			--virt-type kvm \
			--os-variant linux \
			$cpu_args \
			--ram $ARG_MEMORY \
			--network bridge=$DATA_BRIDGE,model=virtio,mac="$vhost_mac" \
			--disk path=$KVM_VM_VDISK \
			--import \
			--graphics none \
			--noautoconsole \
			--channel unix,mode=bind,path=/var/lib/libvirt/qemu/${DOCKER_KVM_NAME}.agent,target_type=virtio,name=org.qemu.guest_agent.0"

			# Host 1/2
			for ssh in "$HOST1_SSH_MANAGEMENT" "$HOST2_SSH_MANAGEMENT"
			do
				ssh $ssh "mkdir $DIR_TMP"
			done

			# Host 1
			echo "Copying VM image file to Host 1 ..."
			scp $DIR_APP_KVM/$APP_KVM_VM_IMAGE $HOST1_SSH_MANAGEMENT:${DIR_TMP} >/dev/null

			;;
	esac

	# Set headers of benchmark file
	echo -ne "${FILE_MEASUREMENTS_HEADER}\n\n${csv_network_capacity}\n\n${csv_header}\n" >> "$FILE_MEASUREMENTS"

	# Host 1/2: Syncing time and copying resource monitor
	echo "Host 1/2: Syncing time and copying resource monitor ..."
	for ssh in "$HOST1_SSH_MANAGEMENT" "$HOST2_SSH_MANAGEMENT"
	do
		{
			ssh $ssh /bin/sh <<-EOF
				$cmd_timesync
			EOF
			scp $DIR_SRC/$FILE_RESMON $ssh:$DIR_TMP
		} >/dev/null
	done

	# Controller: Syncing time
	echo "Controller: Syncing time..."
	ssh $CONTROLLER_SSH "$cmd_timesync" > /dev/null

	# docker stats --format "table {{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}\t{{.NetIO}}"
	#	xfce4-terminal --hide-menubar -T "$HOST1_SSH_MANAGEMENT" -e "ssh $HOST1_SSH_MANAGEMENT -t htop"

	# Host 1/2: Starting resource monitor
	echo "Host 1/2: Starting Resource Monitor ..."
	ssh $HOST1_SSH_MANAGEMENT "sudo $DIR_TMP/$FILE_RESMON -i '$HOST1_IF_DATA' -n '$DOCKER_KVM_NAME' -t '$ARG_TYPE' -v '$T_MONITOR' '$FILE_TMP_RESMON' >/dev/null 2>&1 &"
	ssh $HOST2_SSH_MANAGEMENT "sudo $DIR_TMP/$FILE_RESMON -c '$DIR_TMP/cpuacct.usage_percpu' -i '$HOST2_IF_DATA' -n '$DOCKER_KVM_NAME' -t '$ARG_TYPE' -v '$T_MONITOR' '$FILE_TMP_RESMON' >/dev/null 2>&1 &"

	# Stressing network
	stressNetwork
}
###########################################################################################

###################################### stressNetwork ######################################
# Stresses network and logs the results
#
# Requires:	ARG_STRESS_NET, ARG_STRESS_NET_REV
#		HOST1_IP_DATA, HOST1_SSH_MANAGEMENT, HOST2_IP_DATA, HOST2_SSH_MANAGEMENT
#		LOG_TMP_IPERF
stressNetwork() {
	if ! [ -z $ARG_STRESS_NET ]
	then
		echo "Stressing network host1->host2 ..."
		ssh $HOST2_SSH_MANAGEMENT "iperf3 -s $HOST2_IP_DATA >/dev/null &"
		ssh $HOST1_SSH_MANAGEMENT "iperf3 -c $HOST2_IP_DATA -u -b $ARG_STRESS_NET -t 86400 >$LOG_TMP_IPERF 2>&1 &"
	fi

	if ! [ -z $ARG_STRESS_NET_REV ]
	then
		echo "Stressing network host2->host1 ..."
		ssh $HOST1_SSH_MANAGEMENT "iperf3 -s $HOST1_IP_DATA >/dev/null &"
		ssh $HOST2_SSH_MANAGEMENT "iperf3 -c $HOST1_IP_DATA -u -b $ARG_STRESS_NET_REV -t 86400 >$LOG_TMP_IPERF 2>&1 &"
	fi
}
###########################################################################################

######################################## timestamp ########################################
# Creates a timestamp
#
# Returns:	Current time as an Epoch timestamp (in ms)
timestamp() {
	timestamp=$(date +%s%N)
	timestamp=${timestamp:0:-6}
	echo "$timestamp"
}
###########################################################################################

################################# waitUntilAlive "$1" "$2" ################################
# Waits for a host to be up again after a reboot
#
# Arguments:	1	IP of the host
#		2	port to check if the host is reachable (e.g. 22 for SSH server)
waitUntilAlive() {
	local ip="$1"
	local port="$2"

	nc -z $ip $port > /dev/null
	while [ $? -ne 0 ]
	do
		sleep 1
		nc -z $ip $port > /dev/null
	done
}
###########################################################################################


###########################################################################################
########################################## MAIN ###########################################
###########################################################################################
# X11 server: Allow incoming connections
xhost + > /dev/null
reset; resize -s 30 100 > /dev/null

checkRequirements
setDefaults

# Process passed arguments
while [ $# -gt 0 ]
do
	case $1 in
		-h|--help)
			printUsage
			exit 0
			;;
		-a|--app)
			ARG_APPNAME="$2"
			if ! [ -f "$DIR_APP_ROOT/$ARG_APPNAME/$FILE_APP_CONFIG" ]
			then
				printUsage
				exit 1
			fi
			shift
			;;
		-c|--cpus)
			ARG_CPUS="$2"
			if ! checkParam "ARG_CPUS" "$ARG_CPUS_MIN" "$ARG_CPUS_MAX"
			then
				printUsage
				exit 1
			fi
			shift
			;;
		-d|--dedicated)
			ARG_DEDICATED=1
			;;
		-m|--memory)
			ARG_MEMORY="$2"
			if ! checkParam "ARG_MEMORY" "$ARG_MEMORY_MIN" "$ARG_MEMORY_MAX"
			then
				printUsage
				exit 1
			fi
			shift
			;;
		-r|--run)
			ARG_RUN="$2"
			if ! checkParam "ARG_RUN" "$ARG_RUN_MIN" "$ARG_RUN_MAX"
			then
				printUsage
				exit 1
			fi
			shift
			;;
		-t|--type)
			ARG_TYPE="$2"
			case $ARG_TYPE in
				docker|kvm)
					;;
				*)
					printUsage
					exit 1
					;;
			esac
			shift
			;;
		-w|--downtime)
			ARG_DOWNTIME="$2"
			if ! checkParam "ARG_DOWNTIME" "$ARG_DOWNTIME_MIN" "$ARG_DOWNTIME_MAX"
			then
				printUsage
				exit 1
			fi
			shift
			;;
		-C|--stress-cpu)
			ARG_STRESS_CPU="$2"
			if ! checkParam "ARG_STRESS_CPU" "$ARG_STRESS_CPU_MIN" "$ARG_STRESS_CPU_MAX"
			then
				printUsage
				exit 1
			fi
			shift
			;;
		-M|--stress-mem)
			ARG_STRESS_MEM="$2"
			if ! checkParam "ARG_STRESS_MEM" "0" "$ARG_MEMORY_MAX"
			then
				printUsage
				exit 1
			fi
			shift
			;;
		-N|--stress-net)
			ARG_STRESS_NET="$2"
			if ! checkParam "ARG_STRESS_NET"
			then
				printUsage
				exit 1
			fi
			shift
			;;
		-O|--stress-net-rev)
			ARG_STRESS_NET_REV="$2"
			if ! checkParam "ARG_STRESS_NET_REV"
			then
				printUsage
				exit 1
			fi
			shift
			;;
		*)
			[ $# -ge 1 ] && mkdir -p "$@" >/dev/null 2>&1
			if [ $? -eq 0 ]
			then
				DIR_LOG="$@"
				DIR_NAME="$( echo $DIR_LOG | rev | cut -d'/' -f 1 | rev )"
			else
				printUsage
				exit 1
			fi
			;;
	esac
	shift
done

updateGlobals
printWelcome
setup

# Run test cycles
cycle=1
while [ $cycle -le $ARG_RUN ]
do
	runCycle "$cycle"
	cycle=$(expr $cycle + 1)
done

# End
end

# To remove

# todo
# echo "VM: Clock before migration"
# ssh "$KVM_VM_SSH" "chronyc sources; python3 -c 'import time; print(time.time())'"

# todo
# echo "VM: Clock after migration"
# ssh "$KVM_VM_SSH" "python3 -c 'import time; print(time.time())'; chronyc sources"
