# *Migration Benchmark Script*
---------------------------
*A Live Migration Benchmarking System for docker and KVM*

## *Description*
This repository contains the source code for benchmarking live migration with two off-the-shelf technologies, namely docker containers and KVM.
Live migration is defined as the process of seamlessly relocating a service between physical hosts. The principal of this technology is that the service downtime is small enough so the end user is not aware of the migration. This repository serves as the initial point to set up the testbed and ensure that all measurements are done in comparable and comprehensible way. It fulfils several purposes:

**Testbed Preparation.** It  covers the configuration of the hosts’ network interfaces, loading the container’s/virtual machine’s image, configuring the SDN controller and starting the resource monitor on the hosts to log the CPU, RAM and network load.

**Run.** It runs the live migration process a predefined number of cycles. Each cycle basically consists of four steps: starting the container/VM on the first host, generating CPU/RAM/network load, starting the test application on the client and migrating the container/VM to the second host.

**Measuring & Logging.** It collects different log files and timestamps from the evaluation. Then, it calculates the migration time, downtime, and resources used. Those values are saved in CSV files for further postprocessing.

## *Testbed Setup*

The current script supports the migration under the following setup. There are four computers connected to a SDN switch. One computer acts as a controller, two computers act as the endpoints of the migration, and the fourth computer acts as a client. We configure three different networks, the management network, the controller network, and the data network. They grant internet access to the devices, connect the SDN controller and the SDN switch, and establish a communication for the application, respectively.

### *Important*:
The OpenCV image for KVM is not uploaded to this repository because it was too big (1.2GB). Please download it from [this link](https://drive.google.com/file/d/10W4_ogH2PqxWnWxgnUl7_szMJ2MPuTAx/view?usp=sharing) and save it inside the *kvm* folder. Furhter information can be found [here](https://bitbucket.org/comnets/migration-bechnmark-script/src/master/kvm/download_kvm_img.txt).

## *Benchmark Program Manual*

There exists a pdf manual in this repository (MANUAL.pdf) with all the instruccions needed to set up, and run the benchmark script. 
Please refer to it when using the source code.

## *Useful Links*

#### *Containers*

- [Containers unplugged: Understanding user namespaces](http://man7.org/conf/meetup/understanding-user-namespaces--jambit-Kerrisk-2019-05-28.pdf)
- [Containers unplugged: An introduction to control groups (cgroups) v2](http://man7.org/conf/meetup/conf_cgroups_v2--jambit-Kerrisk-2019-06-05.pdf)

- [Docker Get Started Tutorial](https://docs.docker.com/get-started/)
- [Awesome-docker](https://awesome-docker.netlify.com/)

#### *KVM*

- [KVM Debian Wiki](https://wiki.debian.org/KVM)

#### *SDN*

- [Introduction to Open vSwitch (Video)](https://www.youtube.com/watch?v=rYW7kQRyUvA)
- [Ryu SDN Framework (Free eBook)](https://osrg.github.io/ryu-book/en/html/)

## *License*

ComNets Live Migration Benchmark is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

ComNets Live Migration Benchmark is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ComNets Live Migration Benchmark.  If not, see <https://www.gnu.org/licenses/>.

## *Authors*

Roberto Torre , Robert-Steve Schmoll, Florian Kemser, Hani Salah, Ievgenii Tsokalo, Frank H.P. Fitzek

Deutsche Telekom Chair of Communication Networks

##### *Contact*
Coordinator: Roberto Torre (roberto.torre@tu-dresden.de)

Main developer: Florian Kemser (florian.kemser@mailbox.tu-dresden.de)

